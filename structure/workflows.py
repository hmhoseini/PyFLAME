from fireworks import Workflow
from structure.fireworks import RandomStructGenPyXtalFW, StoreRandomStructFW
from structure.core import get_allowed_n_atom_for_compositions
from workflows.config import *

def get_pyxtal_wf(comp_list):
    wf_name = 'step-1'
    pyxtal_fws = []
    for composition in comp_list:
        allowed_n_atom = get_allowed_n_atom_for_compositions([composition])

        attempts = round((input_list['max_number_of_bulk_structures']/len(input_list['bulk_number_of_atoms']) +\
                     input_list['max_number_of_reference_structures']/len(input_list['reference_number_of_atoms']))*5/230)
        if attempts == 0:
            attempts = 1
        for a_n_a in allowed_n_atom:
            for i in range(attempts):
                pyxtalfw = RandomStructGenPyXtalFW(composition, a_n_a, name='pyxtal_'+str(a_n_a)+'-atoms')
                pyxtal_fws.append(pyxtalfw)
    store_fw = [StoreRandomStructFW(name='store_random_structures', parents = pyxtal_fws)]
    fws = []
    fws = pyxtal_fws + store_fw
    pyxtal_wf = Workflow(fws, name = wf_name)
    return pyxtal_wf
