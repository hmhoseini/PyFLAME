import os
import json
import random
import time
import multiprocessing as mp
import matplotlib.pyplot as plt
from collections import defaultdict
from pymatgen.analysis.molecule_structure_comparator import CovalentRadius
from pymatgen.core.composition import Composition
from pymatgen.core.structure import Structure
from pyxtal import pyxtal
from workflows.core import find_block_folder
from structure.core import is_structure_valid
from flame.flame_functions.io_yaml import read_yaml, atoms2dict
from workflows.config import *

def generate_random_bulk_natom(a_comp, a_n_a):
    covalent_radius = CovalentRadius.radius
    pool = mp.Pool(mp.cpu_count())
    result_objects = []
    elements = []
    n_elmnt = []
    for e, n in Composition(a_comp).items():
        elements.append(str(e))
        n_elmnt.append(int(n))
    n_element = [n_el*int(a_n_a/sum(n_elmnt)) for n_el in n_elmnt]

    if os.path.exists(os.path.join(random_structures_dir,'vpa.dat')):
        with open(os.path.join(random_structures_dir,'vpa.dat'), 'r') as f:
            vpas = [float(line.strip()) for line in f]
    else:
        vol = 0
        for i in range(len(elements)):
            vol += 8 * covalent_radius[elements[i]]**3 * n_elmnt[i]
        vpas[0] = vol/sum(n_elmnt)
        vpas[1] = vol/sum(n_elmnt) * 2
        with open(os.path.join(random_structures_dir,'vpa.dat'), 'w') as f:
            f.writelines("%s\n" % vpas[0])
            f.writelines("%s\n" % vpas[1])

    for space_group in range(1,231):
        result_objects.append(pool.apply_async(generate_random_3d, args=(space_group, elements, n_element, vpas)))

    random_bulk_structures = []
    for r_o in result_objects:
        a_r_structure = r_o.get()
        if a_r_structure:
            random_bulk_structures.append(a_r_structure)

    pool.close()
    pool.join()

    with open(os.path.join('random_bulk_structures.json'),'w') as f:
        json.dump(random_bulk_structures, f)

def generate_random_3d(space_group, elements, n_element, vpas):
    start = time.time()
    t = time.time()
    rand_bulk = pyxtal()
    min_d_prefactor = input_list['min_distance_prefactor']
    while True:
        t = time.time()
        if int(t-start) > 300:
            break
        factor = random.uniform(1,1.1) if sum(n_element) < 50 else random.uniform(1.1,2)
        try:
            rand_bulk.from_random(3, space_group, elements, n_element, factor)
        except:
            continue

        r_bulk = rand_bulk.to_pymatgen()
        vpa = r_bulk.volume/len(r_bulk.sites)
        if vpa < vpas[0] or vpa > vpas[1]:
            random. uniform(vpas[0]/vpa, vpas[1]/vpa)
            r_bulk.scale_lattice(r_bulk.volume * (vpas[0]/vpa))
        if is_structure_valid(r_bulk, min_d_prefactor, True, False):
            return r_bulk.as_dict()
    return None

def store_random_structures():
    random_bulk_structures_dict = defaultdict(list)
    min_d_prefactor = input_list['min_distance_prefactor']

    block_dir = find_block_folder()[0]

    for root, dirs, files in os.walk(os.path.join(output_dir,block_dir)):
        if 'random_bulk_structures.json' in files:
            with open(os.path.join(root,'random_bulk_structures.json'),'r') as f:
                s = json.loads(f.read())
            for a_random_bulk_structure in s:
                random_bulk_structures_dict[len(Structure.from_dict(a_random_bulk_structure).sites)].append(a_random_bulk_structure)
        if 'posout.yaml' in files:
            atoms_all_yaml = []
            try:
                atoms_all_yaml = read_yaml(os.path.join(root,'posout.yaml'))
            except:
                continue
            for atoms in atoms_all_yaml:
                conf = atoms2dict(atoms)
                lattice = conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                a_random_bulk_structure = Structure(lattice,spcs,crdnts,coords_are_cartesian=True)

                if is_structure_valid(a_random_bulk_structure, min_d_prefactor, True, False):
                    random_bulk_structures_dict[len(a_random_bulk_structure.sites)].append(a_random_bulk_structure.as_dict())

    if len(random_bulk_structures_dict.keys()) == 0:
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write('>>> WARNING: no random structure is generated <<<'.format(str(keys))+'\n')
    else:
        for keys in random_bulk_structures_dict.keys():
            if len(random_bulk_structures_dict[keys]) == 0:
                with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
                    f.write('>>> WARNING: no random structure for structures with {} atoms is generated <<<'.format(str(keys))+'\n')
            else:
                with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
                    f.write('{} random bulk structures with {} atoms are generated'.format(len(random_bulk_structures_dict[keys]), str(keys))+'\n')

        with open(os.path.join(random_structures_dir,'random_bulk_structures.json'),'w') as f:
            json.dump(random_bulk_structures_dict, f)

        plot_random_structures()

def plot_random_structures():
    plot_nat =[]
    plot_vpa = []

    with open(os.path.join(random_structures_dir,'random_bulk_structures.json'), 'r') as f:
        s = json.loads(f.read())

    for keys in s.keys():
        for a_structure in s[keys]:
            plot_nat.append(int(keys))
            plot_vpa.append(Structure.from_dict(a_structure).volume/len(Structure.from_dict(a_structure).sites))
    plt.figure(1)
    plt.scatter(plot_nat,plot_vpa, label='vpa-vs-nat')
    plt.xlabel('nat')
    plt.ylabel(r'vpa  ${\AA}^3/atom$')
    plt.savefig(os.path.join(random_structures_dir,'random_structures_vpa-vs-nat.png'))
    plt.close()
