import os
import json
from random import shuffle
from collections import defaultdict
from pymatgen.core.structure import Structure
from pymatgen.core.composition import Composition
from mp_api.client import MPRester
from pymatgen.analysis.molecule_structure_comparator import CovalentRadius
from pymatgen.core.periodic_table import Element
from pymatgen.io.vasp import Poscar
from workflows.config import *

def is_structure_valid(structure, min_d_prefactor, check_angles, check_vpa):
    if check_angles:
        angles = structure.lattice.angles
        if angles[0] > 135 or angles[0] < 45 or\
           angles[1] > 135 or angles[1] < 45 or\
           angles[2] > 135 or angles[2] < 45:
            return False
    if check_vpa:
        with open(os.path.join(random_structures_dir,'vpa.dat'), 'r') as f:
            vpas = [float(line.strip()) for line in f]
        vpa = structure.volume/len(structure.sites)
        if vpa < vpas[0] or vpa > vpas[1]:
            return False

    d_matrix = structure.distance_matrix
    for isites in range(len(structure.sites)):
        if not structure.get_neighbors(structure[isites], 5):
            return False
        if min_d_prefactor:
            for jsites in range(len(structure.sites)):
                min_d = get_min_d(structure[isites].species_string, structure[jsites].species_string) * min_d_prefactor
                if d_matrix[isites,jsites] > 0 and d_matrix[isites,jsites] < min_d:
                    return False
    return True

def get_min_d(elmnt1, elmnt2, X=False):
    covalent_radius = CovalentRadius.radius
    min_d = (covalent_radius[elmnt1] + covalent_radius[elmnt2])
    x_prefactor = 1
    if X:
        try:
            x_prefactor = 1 - (abs(Element(elmnt1).X - Element(elmnt2).X)/(3.98 - 0.79)) * 0.2
        except:
            pass
    return min_d * x_prefactor

def r_cut():
    all_rcuts = []
    n_atom_in_rcut = max(input_list['bulk_number_of_atoms']) * 1.3
    file_name = os.path.join(random_structures_dir,'minhocao_seeds.json')
    with open(file_name, 'r') as f:
        all_structs=json.loads(f.read())
    for struct in all_structs:
        for d in range(6,30):
            if len(Structure.from_dict(struct).get_sites_in_sphere([0,0,0],d)) < n_atom_in_rcut:
                continue
            all_rcuts.append(d)
            break
    return sum(all_rcuts)/len(all_rcuts)

def write_scaled_structure(scale_factor, struct = None):
    if not struct:
        struct = Structure.from_file('./POSCAR')
    struct.scale_lattice(struct.volume*scale_factor)
    Poscar(struct).write_file('POSCAR')

def scale_structure(scale_factor, struct):
    struct.scale_lattice(struct.volume*scale_factor)
    return struct

def write_perturbed_cluster(perturb_dist, struct = None):
    if not struct:
        struct = Structure.from_file('./POSCAR')
    struct.perturb(perturb_dist)
    Poscar(struct).write_file('POSCAR')

def write_compositions_elements(composition_list):
    element_list = []
    for a_composition in composition_list:
        for elmnt in Composition(a_composition).elements:
            if str(elmnt) not in element_list:
                element_list.append(str(elmnt))
    with open(os.path.join(random_structures_dir,'elements.dat'), 'w') as f:
        f.writelines(["%s\n" % el  for el in element_list])
    with open(os.path.join(random_structures_dir,'compositions.dat'), 'w') as f:
        f.writelines(["%s\n" % comp  for comp in composition_list])

def get_known_structures(composition_list):
    mpr= MPRester(config_list['api_key'])
    known_structures = []
    vpas = []
    todump = []
    for a_composition in composition_list:
        docs = mpr.materials.search(formula=a_composition, fields=["structure"])
        known_structures.extend(docs)
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write('Number of known structures in the MPDB for {}: {}'.format(a_composition, len(docs))+'\n')

    for a_k_s in known_structures:
        p_a_k_s = a_k_s.structure.get_primitive_structure()
        vpas.append(p_a_k_s.volume/len(p_a_k_s.sites))

        if len(p_a_k_s.sites) in input_list['bulk_number_of_atoms']+input_list['reference_number_of_atoms']:
            todump.append(p_a_k_s.as_dict())

    if len(todump) > 0:
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write('Number of known structures in the MPDB with given number of atoms:{}'.format(len(todump))+'\n')
        with open(os.path.join(random_structures_dir,'known_bulk_structures.json'),'w') as f:
            json.dump(todump, f)
    else:
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write('>>> WARNING: No bulk structure with given number of atoms was found in the MPDB <<<'+'\n')

    if len(vpas) > 0:
        minmaxvpa = [min(vpas), max(vpas)]
        with open(os.path.join(random_structures_dir,'vpa.dat'), 'w') as f:
            f.writelines(["%s\n" % vpas  for vpas in minmaxvpa])

def read_bulk_structure_from_db(composition_list, file_format, anonymous_formula):
    with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
        f.write('Reading structures from the local database'+'\n')
    min_d_prefactor = input_list['min_distance_prefactor']
    db_bulk_structures_dict = defaultdict(list)
    for a_comp in composition_list:
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write('Composition: {}'.format(a_comp)+'\n')
        n_elmnt = []
        element_list = []
        for e, n in Composition(a_comp).items():
            element_list.extend(int(n) * [str(e)])
            n_elmnt.append(int(n))
        allowed_n_atom = get_allowed_n_atom_for_compositions([a_comp])
        if 'json' in file_format:
            for a_file in os.listdir(db_dir):
                if a_file.endswith('json'):
                    with open(os.path.join(db_dir,a_file), 'r') as f:
                        db_structures = json.loads(f.read())
                    for a_db_structure_dict in db_structures:
                        a_db_structure = Structure.from_dict(a_db_structure_dict)
                        if len(a_db_structure.sites) in allowed_n_atom:
                            if anonymous_formula:
                                elements = int(len(a_db_structure.sites)/sum(n_elmnt)) * element_list
                                shuffle(elements)
                                for i in range(len(a_db_structure.sites)):
                                    a_db_structure.replace(i, elements[i])
                            if a_db_structure.composition.reduced_formula == Composition(a_comp).reduced_formula\
                                                        and is_structure_valid(a_db_structure, min_d_prefactor, True, False):
                                db_bulk_structures_dict[len(a_db_structure.sites)].append(a_db_structure.as_dict())
        if 'vasp' in file_format:
            for a_file in os.listdir(db_dir):
                if a_file.endswith('vasp'):
                    a_db_structure = Structure.from_file(os.path.join(db_dir,a_file))
                    if len(a_db_structure.sites) in allowed_n_atom:
                        if anonymous_formula:
                            elements = int(len(a_db_structure.sites)/sum(n_elmnt)) * element_list
                            shuffle(elements)
                            for i in range(len(a_db_structure.sites)):
                                a_db_structure.replace(i, elements[i])
                        if a_db_structure.composition.reduced_formula == Composition(a_comp).reduced_formula\
                                        and is_structure_valid(a_db_structure, min_d_prefactor, True, False):
                            db_bulk_structures_dict[len(a_db_structure.sites)].append(a_db_structure.as_dict())

    for keys in db_bulk_structures_dict.keys():
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write("{} bulk structures for {}-atom systems are retrived from the local db.".format(len(db_bulk_structures_dict[keys]), keys)+'\n')
    return db_bulk_structures_dict

def get_allowed_n_atom_for_compositions(composition_list):
    allowed_n_atom = []
    for a_comp in composition_list:
        n_elmnt = []
        for e, n in Composition(a_comp).items():
            n_elmnt.append(int(n))
        n_element = n_elmnt
        nmax = 1
        while sum(n_element) <= max(input_list['bulk_number_of_atoms']+input_list['reference_number_of_atoms']):
            if sum(n_element) >= min(input_list['bulk_number_of_atoms']+input_list['reference_number_of_atoms'])\
                        and sum(n_element) not in allowed_n_atom\
                        and sum(n_element) in input_list['bulk_number_of_atoms']+input_list['reference_number_of_atoms']:
                allowed_n_atom.append(sum(n_element))
            nmax = nmax + 1
            n_element = [n_el * nmax for n_el in n_elmnt]
    return allowed_n_atom

def read_composition_list():
    if os.path.exists(os.path.join(random_structures_dir,'compositions.dat')):
        with open(os.path.join(random_structures_dir,'compositions.dat'), 'r') as f:
            composition_list = [line.strip() for line in f]
    else:
        return None
    if len(composition_list) > 0:
        return composition_list
    return None

def read_element_list():
    with open(os.path.join(random_structures_dir,'elements.dat'), 'r') as f:
        element_list = [line.strip() for line in f]
    return element_list
