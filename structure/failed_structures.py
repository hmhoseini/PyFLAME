import os
import re
import json
import numpy as np
from pymatgen.core.structure import Structure, Molecule
from structure.core import is_structure_valid
from workflows.config import *

def get_pertured_failed_structures(c_step_number):
    s_n = int(re.split('-',c_step_number)[1])

    min_d_prefactor = input_list['min_distance_prefactor'] * ((100-float(input_list['descending_prefactor']))/100)**(s_n - 1)\
                      if input_list['descending_prefactor'] else input_list['min_distance_prefactor']

    failed_bstructures = []
    failed_cstructures = []

    f_path = os.path.join(Flame_dir,c_step_number,'minhocao')
    if os.path.exists(os.path.join(f_path,'failed_structures.json')):
        with open(os.path.join(f_path,'failed_structures.json'), 'r') as f:
            f_s = json.loads(f.read())
        failed_bstructures.extend(f_s)

    f_path = os.path.join(Flame_dir,c_step_number,'minhopp')
    if os.path.exists(os.path.join(f_path,'failed_bulk_structures.json')):
        with open(os.path.join(f_path,'failed_bulk_structures.json'), 'r') as f:
            f_s = json.loads(f.read())
        failed_bstructures.extend(f_s)
    if os.path.exists(os.path.join(f_path,'failed_cluster_structures.json')):
        with open(os.path.join(f_path,'failed_cluster_structures.json'), 'r') as f:
            f_s = json.loads(f.read())
        failed_cstructures.extend(f_s)

    perturbed_bstructures = []
    perturbed_cstructures = []

    for a_b_struct in failed_bstructures:
        a_b_structure = Structure.from_dict(a_b_struct)
        for p in (0.02, 0.05, 0.07, 0.10):
            a_b_s = a_b_structure.copy()
            a_b_s.perturb(p)
            if is_structure_valid(a_b_s, min_d_prefactor, False, False):
                perturbed_bstructures.append(a_b_s)

    for a_c_struct in failed_cstructures:
        spcs = Structure.from_dict(a_c_struct).species
        crdnts = Structure.from_dict(a_c_struct).cart_coords
        array_crdnts = np.array(crdnts)
        maxx = max(array_crdnts[:,0:1])[0]
        minx = min(array_crdnts[:,0:1])[0]
        maxy = max(array_crdnts[:,1:2])[0]
        miny = min(array_crdnts[:,1:2])[0]
        maxz = max(array_crdnts[:,2:3])[0]
        minz = min(array_crdnts[:,2:3])[0]
        a_cluster = maxx-minx+input_list['vacuum_length']
        b_cluster = maxy-miny+input_list['vacuum_length']
        c_cluster = maxz-minz+input_list['vacuum_length']
        if a_cluster > input_list['box_size'] or b_cluster > input_list['box_size'] or c_cluster > input_list['box_size']:
            continue
        molecule = Molecule(spcs, crdnts)
        try:
            boxed_molecule = molecule.get_boxed_structure(a_cluster,b_cluster,c_cluster)
        except:
            continue
        for p in (0.02, 0.05, 0.07, 0.10):
            a_c_s = boxed_molecule.copy()
            a_c_s.perturb(p)
            if is_structure_valid(a_c_s, min_d_prefactor, False, False):
                perturbed_cstructures.append(a_c_s)

    return perturbed_bstructures, perturbed_cstructures
