from fireworks import Firework, PyTask

class RandomStructGenPyXtalFW(Firework):
    def __init__(
       self,
       composition,
       a_n_a,
       name,
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='structure.random_structures.generate_random_bulk_natom', args = [composition, a_n_a]))
        super().__init__(t, parents=parents, name = name, **kwargs)

class StoreRandomStructFW(Firework):
    def __init__(
       self,
       name,
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='structure.random_structures.store_random_structures', args = []))
        super().__init__(t, parents=parents, name = name, **kwargs)
