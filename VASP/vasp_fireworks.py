from fireworks import Firework, PyTask
from atomate.vasp.firetasks.glue_tasks import CopyVaspOutputs
from atomate.vasp.firetasks.run_calc import RunVaspCustodian
from atomate.vasp.config import VASP_CMD
from atomate.vasp.firetasks.parse_outputs import VaspToDb
from atomate.common.firetasks.glue_tasks import PassCalcLocs
from atomate.vasp.firetasks.write_inputs import WriteVaspFromPMGObjects, WriteVaspFromIOSet
from workflows.config import *
from custodian.vasp.handlers import FrozenJobErrorHandler

handlers = [FrozenJobErrorHandler(timeout=3600)]

class ScaledStructFW(Firework):
    def __init__(
        self,
        structure=None,
        scale_factor=None,
        name=None,
        task_label=None,
        vasp_input_set=None,
        vasp_cmd=VASP_CMD,
        prev_calc_loc=True,
        db_file=False,
        parents=None,
        **kwargs,
    ):
        t = []
        incar  = vasp_input_set.incar
        potcar = vasp_input_set.potcar
        fw_name = name

        if parents:
            t.append(CopyVaspOutputs(calc_loc=prev_calc_loc, contcar_to_poscar=True))
            t.append(PyTask(func='structure.core.write_scaled_structure', args = [scale_factor]))
        elif structure:
            t.append(PyTask(func='structure.core.write_scaled_structure', args = [scale_factor, structure]))
            t.append(WriteVaspFromPMGObjects(potcar=potcar))
        else:
            raise ValueError("Must specify structure or previous calculation")
        t.append(WriteVaspFromPMGObjects(incar=incar))
        t.append(RunVaspCustodian(vasp_cmd=vasp_cmd, handler_group = handlers, max_errors = 1))
        t.append(PassCalcLocs(name=name))
        t.append(VaspToDb(db_file=db_file, additional_fields={"task_label": task_label}))
        t.append(PyTask(func='workflows.core.extend_t', args = []))
        super().__init__(t, parents=parents, name=fw_name, **kwargs)

class PerturbedClusterFW(Firework):
    def __init__(
        self,
        structure=None,
        perturb_dist=None,
        name=None,
        task_label=None,
        vasp_input_set=None,
        vasp_cmd=VASP_CMD,
        prev_calc_loc=True,
        db_file=False,
        parents=None,
        **kwargs,
    ):
        t = []
        incar  = vasp_input_set.incar
        potcar = vasp_input_set.potcar
        fw_name = name

        if parents:
            t.append(CopyVaspOutputs(calc_loc=prev_calc_loc, contcar_to_poscar=True))
            t.append(PyTask(func='structure.core.write_perturbed_cluster', args = [perturb_dist]))
        elif structure:
            t.append(PyTask(func='structure.core.write_perturbed_cluster', args = [perturb_dist, structure]))
            t.append(WriteVaspFromPMGObjects(potcar=potcar))
        else:
            raise ValueError("Must specify structure or previous calculation")
        t.append(WriteVaspFromPMGObjects(incar=incar))
        t.append(RunVaspCustodian(vasp_cmd=vasp_cmd, handler_group = handlers, max_errors = 1))
        t.append(PassCalcLocs(name=name))
        t.append(VaspToDb(db_file=db_file, additional_fields={"task_label": task_label}))
        t.append(PyTask(func='workflows.core.extend_t', args = []))
        super().__init__(t, parents=parents, name=fw_name, **kwargs)

class RelaxFW(Firework):
    def __init__(
        self,
        structure=None,
        name=None,
        task_label=None,
        vasp_input_set=None,
        vasp_cmd=VASP_CMD,
        prev_calc_loc=True,
        db_file=False,
        parents=None,
        **kwargs,
    ):
        t = []
        incar = vasp_input_set.incar
        kpoints = vasp_input_set.kpoints or None
        fw_name = name

        if parents:
            t.append(CopyVaspOutputs(calc_loc=prev_calc_loc, contcar_to_poscar=True))
            t.append(WriteVaspFromPMGObjects(incar=incar))
        elif structure:
            t.append(WriteVaspFromIOSet(structure=structure, vasp_input_set=vasp_input_set))
        else:
            raise ValueError("Must specify previous calculation or structure")
        if kpoints:
            t.append(WriteVaspFromPMGObjects(kpoints=kpoints))
        t.append(RunVaspCustodian(vasp_cmd=vasp_cmd, handler_group = handlers, max_errors = 1))
        t.append(PassCalcLocs(name=name))
        t.append(VaspToDb(db_file=db_file, additional_fields={"task_label": task_label}))
        t.append(PyTask(func='workflows.core.extend_t', args = []))
        super().__init__(t, parents=parents, name= fw_name, **kwargs)
