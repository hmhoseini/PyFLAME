import os
import yaml
from fireworks import Workflow
from pymatgen.io.vasp.sets import MITRelaxSet
from VASP.vasp_fireworks import ScaledStructFW, PerturbedClusterFW, RelaxFW
from workflows.config import *

with open(os.path.join(VASP_input_file_path,'INCAR.yaml'), 'r') as f:
    uis = yaml.load(f, Loader=yaml.FullLoader)
with open(os.path.join(VASP_input_file_path,'KPOINTS.yaml'), 'r') as f:
    uks = yaml.load(f, Loader=yaml.FullLoader)
with open(os.path.join(VASP_input_file_path,'POTCAR.yaml'), 'r') as f:
    ups = yaml.load(f, Loader=yaml.FullLoader)

def get_reference_optimization_wf(structs, wf_name):
    wf_list = []
    vasp_fws = []
    priority = 5
    for a_struct in structs:
        opt1_fw = get_opt1_fw(a_struct, 'bulk', priority=priority)
        vasp_fws.append(opt1_fw)

        opt1vc_fw = get_opt1vc_prev_fw(a_struct, priority=priority, parents=vasp_fws[-1])
        vasp_fws.append(opt1vc_fw)

        bulk_fw = get_bulk_fw(a_struct, priority=priority, parents=vasp_fws[-1])
        vasp_fws.append(bulk_fw)

    vasp_fws_split = [vasp_fws[i:i+210] for i in range(0, len(vasp_fws), 210)]
    for i in range(len(vasp_fws_split)):
        wf_list.append(Workflow(vasp_fws_split[i], name = wf_name, metadata = None))
    return wf_list

def get_scheme1_optimization_wf(structs, wf_name):
    wf_list = []
    vasp_fws_bulk = []
    for a_struct in structs:
        nat = len(a_struct.sites)
        if nat > 70:
            priority = 4
        elif nat > 40:
            priority = 3
        else:
            priority = 2

        vasp_fws = []
        opt1_fw = get_opt1_fw(a_struct, 'bulk', priority=priority)
        vasp_fws.append(opt1_fw)

        opt2_fw = get_opt2_fw(a_struct, 'bulk', priority=priority, parents=vasp_fws[-1])
        vasp_fws.append(opt2_fw)

        scaled_bulk_fws = get_scaled_bulk_fws(a_struct, priority=priority, parents=vasp_fws[-1])
        vasp_fws.extend(scaled_bulk_fws)

        vasp_fws_bulk.extend(vasp_fws)

    vasp_fws_bulk_split = [vasp_fws_bulk[i:i+204] for i in range(0, len(vasp_fws_bulk), 204)]
    for i in range(len(vasp_fws_bulk_split)):
        wf_list.append(Workflow(vasp_fws_bulk_split[i], name = wf_name, metadata = None))
    return wf_list

def get_scheme2_optimization_wf(structs, wf_name):
    wf_list = []
    vasp_fws_bulk = []
    for a_struct in structs:
        nat = len(a_struct.sites)
        if nat > 70:
            priority = 4
        elif nat > 40:
            priority = 3
        else:
            priority = 2

        vasp_fws = []
        opt1_fw = get_opt1_fw(a_struct, 'bulk', priority=priority)
        vasp_fws.append(opt1_fw)

        opt1vc_fw = get_opt1vc_prev_fw(a_struct, priority=priority, parents=vasp_fws[-1])
        vasp_fws.append(opt1vc_fw)

        nsw0_b_fw = get_nsw0_prev_fw(a_struct, 'bulk', priority=priority, parents=vasp_fws[-1])
        vasp_fws.append(nsw0_b_fw)

        scaled_bulk_fws = get_scaled_bulk_fws(a_struct, priority=priority, parents=vasp_fws[-1])
        vasp_fws.extend(scaled_bulk_fws)
    
        vasp_fws_bulk.extend(vasp_fws)

    vasp_fws_bulk_split = [vasp_fws_bulk[i:i+210] for i in range(0, len(vasp_fws_bulk), 210)]
    for i in range(len(vasp_fws_bulk_split)):
        wf_list.append(Workflow(vasp_fws_bulk_split[i], name = wf_name, metadata = None))
    return wf_list

def get_cluster_optimization_wf(clusters, wf_name):
    wf_list = []
    vasp_fws_cluster = []
    for a_cluster in clusters:
        nat = len(a_cluster.sites)
        if nat > 70:
            priority = 4
        elif nat > 40:
            priority = 3
        else:
            priority = 2

        vasp_fws = []
        opt1_cluster_fw = get_opt1_fw(a_cluster, 'cluster', priority=priority)
        vasp_fws.append(opt1_cluster_fw)

        opt2_cluster_fw = get_opt2_fw(a_cluster, 'cluster', priority=priority, parents=vasp_fws[-1])
        vasp_fws.append(opt2_cluster_fw)

        vasp_fws_cluster.extend(vasp_fws)

    vasp_fws_cluster_split = [vasp_fws_cluster[i:i+200] for i in range(0, len(vasp_fws_cluster), 200)]
    for i in range(len(vasp_fws_cluster_split)):
        wf_list.append(Workflow(vasp_fws_cluster_split[i], name = wf_name, metadata = None))
    return wf_list

def get_nsw0_wf(structs, wf_name, jb_tp):
    wf_list = []
    vasp_fws = []
    for a_struct in structs:
        nsw0_fw = get_nsw0_scratch_fw(a_struct, job_type=jb_tp)
        vasp_fws.append(nsw0_fw)
    vasp_fws_split = [vasp_fws[i:i+100] for i in range(0, len(vasp_fws), 100)]
    for i in range(len(vasp_fws_split)):
        wf_list.append(Workflow(vasp_fws_split[i], name = wf_name, metadata = None))
    return wf_list

def get_opt1_fw(structure, job_type, **keywords):
    user_incar_settings = uis['opt1'].copy()
    user_incar_settings['LORBIT'] = None
    user_incar_settings['MAGMOM'] = None
    user_incar_settings['ENCUT']  = None
    f_n = 'opt1b'
    if job_type == 'cluster':
        user_incar_settings['KSPACING'] = 5
        user_incar_settings['EDIFFG'] = -0.5
        user_incar_settings['KPAR'] = 1
        f_n = 'opt1c'

    user_potcar_settings = {}
    if ups['POTCAR']:
        for elmnt in ups['POTCAR']:
            user_potcar_settings[elmnt] = ups['POTCAR'][elmnt]

    vis = MITRelaxSet(structure, user_incar_settings = user_incar_settings, user_potcar_settings = user_potcar_settings, potcar_functional=ups['POTCAR_FUNCTIONAL'])
    if uks:
        v = vis.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis = vis.__class__.from_dict(v)

    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), f_n)
    fw = RelaxFW(structure = structure, vasp_input_set = vis, name = fw_name, task_label = f_n, spec={'_priority': keywords['priority']})
    return fw

def get_opt1vc_prev_fw(structure, **keywords):
    user_incar_settings = uis['opt1vc'].copy()
    user_incar_settings['LORBIT'] = None
    user_incar_settings['MAGMOM'] = None

    user_potcar_settings = {}
    if ups['POTCAR']:
        for elmnt in ups['POTCAR']:
            user_potcar_settings[elmnt] = ups['POTCAR'][elmnt]

    vis = MITRelaxSet(structure, user_incar_settings = user_incar_settings, user_potcar_settings = user_potcar_settings, potcar_functional=ups['POTCAR_FUNCTIONAL'])
    if uks:
        v = vis.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis = vis.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'opt1vc_pr')
    fw = RelaxFW(vasp_input_set = vis, name = fw_name, task_label = 'opt1vc_pr', parents = keywords['parents'], spec={'_allow_fizzled_parents': True, '_priority': keywords['priority']})
    return fw

def get_opt2_fw(structure, job_type, **keywords):
    user_incar_settings = uis['opt2'].copy()
    user_incar_settings['LORBIT'] = None
    user_incar_settings['MAGMOM'] = None
    if job_type == 'cluster':
        user_incar_settings['KSPACING'] = 5
        user_incar_settings['EDIFFG'] = -0.3
        user_incar_settings['KPAR'] = 1

    user_potcar_settings = {}
    if ups['POTCAR']:
        for elmnt in ups['POTCAR']:
            user_potcar_settings[elmnt] = ups['POTCAR'][elmnt]

    vis = MITRelaxSet(structure, user_incar_settings = user_incar_settings, user_potcar_settings = user_potcar_settings, potcar_functional=ups['POTCAR_FUNCTIONAL'])
    if uks:
        v = vis.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis = vis.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'opt2_'+job_type)
    fw = RelaxFW(vasp_input_set = vis, name = fw_name, task_label = 'opt2_'+job_type, parents = keywords['parents'], spec = {'_allow_fizzled_parents': True, '_priority': keywords['priority']})
    return fw

def get_bulk_fw(structure, **keywords):
    user_incar_settings = uis['bulk'].copy()
    user_incar_settings['LORBIT'] = None
    user_incar_settings['MAGMOM'] = None

    user_potcar_settings = {}
    if ups['POTCAR']:
        for elmnt in ups['POTCAR']:
            user_potcar_settings[elmnt] = ups['POTCAR'][elmnt]

    vis = MITRelaxSet(structure, user_incar_settings = user_incar_settings, user_potcar_settings = user_potcar_settings, potcar_functional=ups['POTCAR_FUNCTIONAL'])
    if uks:
        v = vis.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis = vis.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'bulk')
    fw = RelaxFW(vasp_input_set = vis, name = fw_name, task_label = 'bulk', parents = keywords['parents'], spec = {'_allow_fizzled_parents': True, '_priority': keywords['priority']})
    return fw

def get_scaled_bulk_fws(structure, **keywords):
    user_incar_settings = uis['opt2'].copy()
    user_incar_settings['LORBIT'] = None
    user_incar_settings['MAGMOM'] = None

    user_potcar_settings = {}
    if ups['POTCAR']:
        for elmnt in ups['POTCAR']:
            user_potcar_settings[elmnt] = ups['POTCAR'][elmnt]

    vis = MITRelaxSet(structure, user_incar_settings = user_incar_settings, user_potcar_settings = user_potcar_settings, potcar_functional=ups['POTCAR_FUNCTIONAL'])
    if uks:
        v = vis.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis = vis.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'scaled_bulk')
    fws = []
    for s_f in (0.85, 0.90, 1.10, 1.20):
        s_b_fw = ScaledStructFW(vasp_input_set = vis, scale_factor = s_f, name = fw_name, task_label = 'scaled_bulk', parents = keywords['parents'], spec = {'_allow_fizzled_parents': True, '_priority': keywords['priority']})
        fws.append(s_b_fw)
    return fws

def get_nsw0_scratch_fw(structure, job_type, **keywords):
    user_incar_settings = uis['nsw0'].copy()
    user_incar_settings['LORBIT'] = None
    user_incar_settings['MAGMOM'] = None
    if job_type == 'cluster' or job_type == 'cfailed':
        user_incar_settings['KSPACING'] = 5
        user_incar_settings['KPAR'] = 1

    user_potcar_settings = {}
    if ups['POTCAR']:
        for elmnt in ups['POTCAR']:
            user_potcar_settings[elmnt] = ups['POTCAR'][elmnt]

    vis = MITRelaxSet(structure, user_incar_settings = user_incar_settings, user_potcar_settings = user_potcar_settings, potcar_functional=ups['POTCAR_FUNCTIONAL'])
    if uks:
        v = vis.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis = vis.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'nsw0_'+job_type)
    fw = RelaxFW(structure = structure, vasp_input_set = vis, name = fw_name, task_label = 'nsw0_'+job_type)
    return fw

def get_nsw0_prev_fw(structure, job_type, **keywords):
    user_incar_settings = uis['nsw0'].copy()
    user_incar_settings['LORBIT'] = None
    user_incar_settings['MAGMOM'] = None

    user_potcar_settings = {}
    if ups['POTCAR']:
        for elmnt in ups['POTCAR']:
            user_potcar_settings[elmnt] = ups['POTCAR'][elmnt]

    vis = MITRelaxSet(structure, user_incar_settings = user_incar_settings, user_potcar_settings = user_potcar_settings, potcar_functional=ups['POTCAR_FUNCTIONAL'])
    if uks:
        v = vis.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis = vis.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'nsw0'+job_type)
    fw = RelaxFW(vasp_input_set = vis, name = fw_name, task_label = 'nsw0_'+job_type, parents = keywords['parents'], spec = {'_allow_fizzled_parents': True, '_priority': keywords['priority']})
    return fw
