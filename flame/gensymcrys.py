import os
import yaml
from random import randint
from itertools import combinations_with_replacement
from pymatgen.analysis.molecule_structure_comparator import CovalentRadius
from structure.core import get_min_d
from workflows.config import *

def write_gensymcrys_files(elements, n_elmnt, a_n_a):
    with open(os.path.join(FLAME_input_file_path,'flame_in.yaml'), 'r') as f:
        flame_in = yaml.load(f, Loader=yaml.FullLoader)
    input_file = flame_in['gensymcrys']
    input_file['main']['types'] = ' '.join(elements)
    r_n = randint(1, 6)
    input_file['main']['seed'] = randint(1, 10**r_n)

    min_d_prefactor = input_list['min_distance_prefactor']

    covalent_radius = CovalentRadius.radius

    if os.path.exists(os.path.join(random_structures_dir,'vpa.dat')):
        with open(os.path.join(random_structures_dir,'vpa.dat'), 'r') as f:
            vpas = [float(line.strip()) for line in f]
    else:
        vol = 0
        vpas = []
        for i in range(len(elements)):
            vol += 8 * covalent_radius[elements[i]]**3 * n_elmnt[i]
        vpas.append(vol/sum(n_elmnt))
        pre_fac = 2 if vpas[0] < 10 else 1.5
        vpas.append(vol/sum(n_elmnt) * pre_fac)
        with open(os.path.join(random_structures_dir,'vpa.dat'), 'w') as f:
            f.writelines("%s\n" % vpas[0])
            f.writelines("%s\n" % vpas[1])

    input_file['genconf']['volperatom_bounds'] = [vpas[0], vpas[1]]
    input_file['genconf']['nat_types_fu'] = n_elmnt
    input_file['genconf']['list_fu'] = [int(a_n_a/sum(n_elmnt))]
    input_file['genconf']['nconf'] = 230
    pairs = {}
    for a_pair in combinations_with_replacement(elements,2):
        pairs[''.join(a_pair)] = get_min_d(a_pair[0], a_pair[1], False) * min_d_prefactor
    input_file['genconf']['rmin_pairs'] = pairs

    with open('flame_in.yaml', 'w') as f:
        yaml.dump(input_file,f)
