import os
import re
import yaml
from random import randint
from itertools import combinations_with_replacement
from pymatgen.core.periodic_table import Element
from pymatgen.core.structure import Structure
from structure.core import get_allowed_n_atom_for_compositions, r_cut
from workflows.config import *

def write_p_f_from_list(struct_list, bc_list, energy_list, force_list, file_name): # structure as dict, boundary condiction, energy in eV
    todump_list = []
    for s_i in range(len(struct_list)):
        todump_dict = {}
        todump_dict = {'conf':{}}
        lattice = Structure.from_dict(struct_list[s_i]).lattice.matrix
        sites   = Structure.from_dict(struct_list[s_i]).sites
        energy  = energy_list[s_i]
        todump_dict['conf']['bc'] = bc_list[s_i]
        todump_dict['conf']['cell'] = []
        todump_dict['conf']['cell'].append([float(lattice[0][0]),float(lattice[0][1]),float(lattice[0][2])])
        todump_dict['conf']['cell'].append([float(lattice[1][0]),float(lattice[1][1]),float(lattice[1][2])])
        todump_dict['conf']['cell'].append([float(lattice[2][0]),float(lattice[2][1]),float(lattice[2][2])])
        todump_dict['conf']['coord'] = []
        for i in range(len(sites)):
            elements = struct_list[s_i]['sites'][i]['species'][0]['element']
            todump_dict['conf']['coord'].append([float(sites[i].x), float(sites[i].y), float(sites[i].z), elements, 'TTT'])
        todump_dict['conf']['epot'] = energy*0.036749309
        if force_list:
            forces = force_list[s_i]
            todump_dict['conf']['force'] = []
            for i in range(len(forces)):
                todump_dict['conf']['force'].append([forces[i][0]*0.01944689673, forces[i][1]*0.01944689673, forces[i][2]*0.01944689673])
        todump_dict['conf']['nat'] = len(sites)
        todump_dict['conf']['units_length'] = 'angstrom'
        todump_list.append(todump_dict)
    with open(file_name, 'w') as f:
        yaml.dump_all(todump_list, f, default_flow_style=None)

def write_SE_ann_input(elmnt_list, step_number):
    rcut = r_cut() * 1.88973

    if step_number:
        if input_list['user_specified_FLAME_files'] and os.path.exists(os.path.join(run_dir,'flame_files','ann_input.yaml')):
            with open(os.path.join(run_dir,'flame_files','ann_input.yaml'), 'r') as f:
                ann_input = yaml.load(f, Loader=yaml.FullLoader)
        else:
            with open(os.path.join(Flame_dir,step_number,'train','ann_input.yaml'), 'r') as f:
                ann_input = yaml.load(f, Loader=yaml.FullLoader)
        s_n = int(re.split('-',step_number)[1])
        number_of_nodes = input_list['number_of_nodes'][s_n - 1]
    else:
        with open(os.path.join(PyFLAME_directory,'flame','flame_files','ann_input.yaml'), 'r') as f:
            ann_input = yaml.load(f, Loader=yaml.FullLoader)
        number_of_nodes = 10

    g02_list = ann_input['g02']
    g05_list = ann_input['g05']

    ener_ref = 0.0
    method = input_list['method']
    combinations_index = list(combinations_with_replacement(range(len(elmnt_list)),2))
    for elmnt in elmnt_list:
        f_name = str(elmnt) + '.ann.input.yaml'
        with open(f_name, 'a') as f:
            f.write('main:'+'\n')
            f.write('    nodes: [{},{}]'.format(number_of_nodes,number_of_nodes)+'\n')
            f.write('    rcut:        {}'.format(rcut)+'\n')
            f.write('    ener_ref:    {}'.format(ener_ref)+'\n')
            f.write('    method:      {}'.format(method)+'\n'+'\n')
            f.write('symfunc:'+'\n')
            n = 0
            for g in g02_list:
                for i in range(len(elmnt_list)):
                    n = n + 1
                    f.write('    g02_{:03d}: {}    {}'.format(n,g,elmnt_list[i])+'\n')
            n = 0
            for g in g05_list:
                for i in range(len(combinations_index)):
                    n = n + 1
                    f.write('    g05_{:03d}: {}    {}    {}'.format(n,g,elmnt_list[combinations_index[i][0]],\
                                                                        elmnt_list[combinations_index[i][1]])+'\n')
def write_FLAME_input_file(job_type, element_list, **keywords):
    with open(os.path.join(FLAME_input_file_path,'flame_in.yaml'), 'r') as f:
        flame_in = yaml.load(f, Loader=yaml.FullLoader)
    input_file = flame_in[job_type]
    input_file['main']['types'] = ' '.join(element_list)

    if 'min_d_prefactor' in keywords:
        min_d_prefactor = keywords['min_d_prefactor']
    if 'step_number' in keywords:
        s_n = int(re.split('-', keywords['step_number'])[1])
    if 'structure' in keywords:
        structure = keywords['structure']
    if 'nconf_rmse' in keywords:
        nconf_rmse = keywords['nconf_rmse']
    if 'composition_list' in keywords:
        composition_list = keywords['composition_list']
        allowed_n_atom = get_allowed_n_atom_for_compositions(composition_list)
        min_n = min(allowed_n_atom)
        max_n = max(allowed_n_atom)

    if job_type == 'aver_dist' or job_type == 'divcheck':
        pass
    elif job_type == 'train':
        r_n = randint(2, 6)
        input_file['main']['seed'] = randint(10, 10**r_n)
        try:
            input_file['ann']['nstep_opt'] = input_list['number_of_epoch'][s_n - 1]
        except:
            input_file['ann']['nstep_opt'] = 20
        input_file['ann']['nconf_rmse'] = nconf_rmse
    elif job_type == 'minhocao':
        r_n = randint(2, 6)
        input_file['main']['seed'] = randint(10, 10**r_n)
        site_symbols = []
        for site in structure:
            site_symbols.append(site.specie.symbol)
        input_file['main']['nat'] = len(structure.sites)
        dummy_typat = []
        for i in range(len(element_list)):
            dummy_typat.append('{}*{}'.format(site_symbols.count(element_list[i]),i+1))
        input_file['main']['typat'] = ' '.join(dummy_typat)
        znucl = []
        for i in range(len(element_list)):
            znucl.append(Element(element_list[i]).Z)
        input_file['main']['znucl'] = znucl
        amass = []
        for i in range(len(element_list)):
            eam = str(Element(element_list[i]).atomic_mass)
            amass.append(float(re.sub(' amu','',eam)))
        input_file['main']['amass'] = amass
        min_t = input_list['minhocao_time'][0]
        max_t = input_list['minhocao_time'][1]
        minhocao_time = min_t + (max_t - min_t)/(max_n - min_n) * (len(structure.sites) - min_n)
        input_file['main']['time_limit'] = minhocao_time
        input_file['minhopp']['nstep'] = input_list['minhocao_steps'][s_n - 1]
    elif job_type == 'minhopp':
        r_n = randint(2, 6)
        input_file['main']['seed'] = randint(10, 10**r_n)
        min_t = input_list['minhopp_time'][0]
        max_t = input_list['minhopp_time'][1]
        minhopp_time = min_t + (max_t - min_t)/(max_n - min_n) * (len(structure.sites) - min_n)
        input_file['main']['time_limit'] = minhopp_time
        input_file['minhopp']['nstep'] = input_list['minhopp_steps'][s_n - 1]
    else:
        return False
    with open('flame_in.yaml', 'w') as f:
        yaml.dump(input_file,f)
