import os
from fireworks import Firework, PyTask
from flame.run import RunFlameCustodian
from workflows.config import *

__author__ = "Sai Ram Kuchana and Hossein Mirhosseini"
__maintainer__ = "Hossein Mirhosseini"
__email__ = "mirhosse@mail.uni-paderborn.de"

class GensymcrysFW(Firework):
    def __init__(
       self,
       elements,
       n_elmnt,
       a_n_a,
       name,
       flame_cmd = ">>flamep_cmd<<",
       parents=None,
       **kwargs,
    ):

        t = []
        t.append(PyTask(func='flame.gensymcrys.write_gensymcrys_files', args = [elements, n_elmnt, a_n_a]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd))
        super().__init__(t, parents = parents, name = name, **kwargs)

class AverDistFW(Firework):
    def __init__(
       self,
       a_n_a,
       name,
       flame_cmd = ">>flamep_cmd<<",
       parents=None,
       **kwargs,
    ):
        t = []
        t.append(PyTask(func='flame.aver_dist.write_aver_dist_files', args = [a_n_a]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd))
        super().__init__(t, parents = parents, name = name, **kwargs)

class Train_pre_FW(Firework):
    def __init__(
       self,
       step_number,
       name,
       parents=None,
       **kwargs,
    ):
        t = []
        t.append(PyTask(func='flame.train.collect_training_data', args = [step_number]))
        t.append(PyTask(func='flame.train.store_symmetry_function', args = [step_number]))
        super().__init__(t, parents = parents, name = name, **kwargs)

class TrainFW(Firework):
    def __init__(
       self,
       step_number,
       name,
       train_type,
       flame_cmd = ">>flamep_cmd<<",
       parents=None,
       **kwargs,
    ):
        t = []
        t.append(PyTask(func='flame.train.write_train_files', args = [step_number]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd, job_type = train_type))
        super().__init__(t, parents = parents, name = name, **kwargs)


class Train_post_FW(Firework):
    def __init__(
       self,
       step_number,
       name,
       parents=None,
       **kwargs,
    ):
        t = []
        t.append(PyTask(func='flame.train.select_a_train', args = [step_number]))
        super().__init__(t, parents = parents, name = name, **kwargs)

class MinhocaoFW(Firework):
    def __init__(
       self,
       structure,
       step_number,
       name,
       flame_cmd = ">>flame_cmd<<",
       parents=None,
       **kwargs,
    ):
        t = []
        t.append(PyTask(func='flame.minhocao.write_minhocao_files', args = [structure, step_number]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd, job_type = 'minhocao'))
        super().__init__(t, parents = parents, name = name, **kwargs)

class MinhocaoStoreFW(Firework):
    def __init__(
       self,
       step_number,
       name,
       parents=None,
       **kwargs,
    ):
        t = []
        t.append(PyTask(func='flame.minhocao.store_minhocao_results', args = [step_number]))
        super().__init__(t, parents = parents, name = name, **kwargs)


class MinhoppFW(Firework):
    def __init__(
       self,
       structure,
       step_number,
       name,
       job_type,
       flame_cmd = ">>flame_cmd<<",
       parents=None,
       **kwargs,
    ):
        t = []
        t.append(PyTask(func='flame.minhopp.write_minhopp_files', args = [structure, step_number, job_type]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd, job_type = 'minhopp'))
        super().__init__(t, parents = parents, name = name, **kwargs)

class MinhoppStoreFW(Firework):
    def __init__(
       self,
       step_number,
       name,
       parents=None,
       **kwargs,
    ):
        t = []
        t.append(PyTask(func='flame.minhopp.store_minhopp_results', args = [step_number]))
        super().__init__(t, parents = parents, name = name, **kwargs)

class DivCheckbFW(Firework):
    def __init__(
       self,
       number_of_atom,
       step_number,
       name,
       dtol,
       flame_cmd = ">>flamep_cmd<<",
       parents=None,
       **kwargs,
    ):
        t = []
        t.append(PyTask(func='flame.divcheck.write_divcheck_b_files', args = [step_number, number_of_atom]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd, job_type = 'divcheck'))
        t.append(PyTask(func='flame.divcheck.run_pickdiff', args = [number_of_atom, dtol]))
        t.append(PyTask(func='shutil.copy', args =['./checked_position_force.json', os.path.join(Flame_dir,step_number,'divcheck', str(number_of_atom)+'-atom_bulk.json')]))
        super().__init__(t, parents = parents, name = name, **kwargs)

class DivCheckcFW(Firework):
    def __init__(
       self,
       number_of_atom,
       step_number,
       name,
       dtol,
       flame_cmd = ">>flamep_cmd<<",
       parents=None,
       **kwargs,
    ):
        t = []
        t.append(PyTask(func='flame.divcheck.write_divcheck_c_files', args = [step_number, number_of_atom]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd, job_type = 'divcheck'))
        t.append(PyTask(func='flame.divcheck.run_pickdiff', args = [number_of_atom, dtol]))
        t.append(PyTask(func='shutil.copy', args =['./checked_position_force.json', os.path.join(Flame_dir,step_number,'divcheck', str(number_of_atom)+'-atom_cluster.json')]))
        super().__init__(t, parents = parents, name = name, **kwargs)
