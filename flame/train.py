import os
import yaml
import json
import re
import math
import shutil
import numpy as np
import multiprocessing as mp
from scipy.optimize import fsolve
from collections import defaultdict
import matplotlib.pyplot as plt
from itertools import combinations
from random import sample
from pymatgen.core.structure import Structure
from structure.core import read_element_list, r_cut
from workflows.core import find_block_folder
from flame.core import write_p_f_from_list, write_SE_ann_input, write_FLAME_input_file
from workflows.config import *

def write_train_files(step_number):
    with open(os.path.join(Flame_dir,step_number,'train','position_force_train_all.json'), 'r') as f:
        training_set = json.loads(f.read())
    indices_list = list(range(len(training_set)))
    valid_indices = sample(indices_list, int(len(indices_list)/5)) if int(len(indices_list)/5) < 10001 else sample(indices_list, 10000)
    for rem in valid_indices:
        indices_list.remove(rem)
    train_indices = indices_list

    f_name ='position_force_train_valid.yaml'
    valid_structure_list = []
    valid_energy_list = []
    valid_force_list = []
    valid_bc_list = []
    for i in valid_indices:
        valid_structure_list.append(training_set[i]['structure'])
        valid_energy_list.append(training_set[i]['energy'])
        valid_force_list.append(training_set[i]['forces'])
        valid_bc_list.append(training_set[i]['bc'])
    write_p_f_from_list(valid_structure_list, valid_bc_list, valid_energy_list, valid_force_list, f_name)
    with open('list_posinp_valid.yaml', 'a') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_train_valid.yaml'+'\n')
    with open('list_posinp_train.yaml', 'a') as f:
        f.write('files:'+'\n')
    t = int(len(indices_list)/10000)
    samp = int(len(indices_list)/(t+1))
    for i in range(1, t+2):
        f_name = 'position_force_train_train_'+'t'+str(i).zfill(3)+'.yaml'
        train_indices_t = sample(train_indices,samp)
        train_structure_list = []
        train_energy_list = []
        train_force_list = []
        train_bc_list = []

        for rem in train_indices_t:
            train_structure_list.append(training_set[rem]['structure'])
            train_energy_list.append(training_set[rem]['energy'])
            train_force_list.append(training_set[rem]['forces'])
            train_bc_list.append(training_set[rem]['bc'])
            train_indices.remove(rem)
        write_p_f_from_list(train_structure_list, train_bc_list, train_energy_list, train_force_list, f_name)
        with open('list_posinp_train.yaml', 'a') as f:
            f.write(' - {}'.format('position_force_train_train_'+'t'+str(i).zfill(3)+'.yaml'+'\n'))
    elmnt_list = read_element_list()
    write_SE_ann_input(elmnt_list, step_number)
    write_FLAME_input_file('train', elmnt_list, nconf_rmse = len(valid_structure_list), step_number = step_number)

def select_a_train(step_number):
    s_n = int(re.split('-',step_number)[1])
    try:
        number_of_epoch = input_list['number_of_epoch'][s_n - 1]
    except:
        number_of_epoch = None
    trains = defaultdict(list)
    elmnt_list = read_element_list()

    block_dir = find_block_folder()[0]

    train_number = 0
    for root, dirs, files in os.walk(block_dir):
        if 'train_output.yaml' in files:
            valid_rmse = []
            with open(os.path.join(root,'train_output.yaml'), 'r') as f:
                t_o = yaml.load(f, Loader=yaml.FullLoader)
            for i in range(len(t_o['training iterations'])):
                try:
                    valid_rmse.append(t_o['training iterations'][i]['valid']['rmse'])
                except:
                    pass
            trains[root].extend(valid_rmse)
            train_number = train_number + 1
            shutil.copyfile(os.path.join(root,'train_output.yaml'),\
                            os.path.join(Flame_dir,step_number,'train','train_number_'+str(train_number)+'_train_output.yaml'))
            if number_of_epoch:
                for elmnt in elmnt_list:
                    try:
                        shutil.copyfile(os.path.join(root,str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5)),\
                                        os.path.join(Flame_dir,step_number,'train','train_number_'+str(train_number)+'_'+str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5)))
                    except:
                        with open(log_file, 'a') as f:
                            f.write('>>> ERROR: no ann potential for epoch {} was found <<<'.format(number_of_epoch)+'\n')
                        exit()

            else:
                n_epoch = valid_rmse.index(min(valid_rmse))
                for elmnt in elmnt_list:
                    shutil.copyfile(os.path.join(root,str(elmnt)+'.ann.param.yaml.'+str(n_epoch).zfill(5)),\
                                    os.path.join(Flame_dir,step_number,'train','train_number_'+str(train_number)+'_'+str(elmnt)+'.ann.param.yaml'+str(n_epoch).zfill(5)))

    rmse_min = 100
    tpath = False
    if number_of_epoch:
        for keys in trains:
            try:
                if trains[keys][number_of_epoch] < rmse_min:
                    rmse_min = trains[keys][number_of_epoch]
                    tpath = keys
            except IndexError:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no RMSE for epoch {} was found <<<'.format(number_of_epoch)+'\n')
                continue
        if tpath:
            for elmnt in elmnt_list:
                shutil.copyfile(os.path.join(tpath,str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5)),\
                                os.path.join(Flame_dir,step_number,'train',str(elmnt)+'.ann.param.yaml'))
            shutil.copyfile(os.path.join(tpath,'train_output.yaml'), os.path.join(Flame_dir,step_number,'train','train_output.yaml'))
    else:
        for keys in trains:
            if min(trains[keys]) < rmse_min:
                rmse_min = min(trains[keys])
                tpath = keys
                n_epoch =  trains[keys].index(rmse_min)
        for elmnt in elmnt_list:
            shutil.copyfile(os.path.join(tpath,str(elmnt)+'.ann.param.yaml.'+str(n_epoch).zfill(5)),\
                            os.path.join(Flame_dir,step_number,'train',str(elmnt)+'.ann.param.yaml'))
        shutil.copyfile(os.path.join(tpath,'train_output.yaml'), os.path.join(Flame_dir,step_number,'train','train_output.yaml'))

    plot_2_train(step_number)

def collect_training_data(step_number):
    training_set = []
    di = {}

    plot_nat_b = []
    plot_epa_b = []
    plot_vpa_b = []

    plot_nat_c = []
    plot_epa_c = []

    s_n = int(re.split('-',step_number)[1])
    p_step_path  = os.path.join(Flame_dir,'step-'+str(s_n - 1))
    with open(os.path.join(random_structures_dir,'min_epa.dat'), 'r') as f:
        min_epa = float(f.readline().strip())
    e_window = input_list['energy_window']
    for root, dirs, files in os.walk(os.path.join(p_step_path,'task_files')):
        for a_taskfile in files:
            with open(os.path.join(root,a_taskfile), 'r') as f:
                s = json.loads(f.read())
            if float(s['output']['energy_per_atom']) < min_epa + e_window:
                di['structure'] = s['output']['structure']
                di['forces']    = s['output']['forces']
                di['energy']    = s['output']['energy']
                if 'cluster' in s['task_label'] or 'cfailed' in s['task_label']:
                    di['bc'] = 'free'
                    plot_nat_c.append(len(Structure.from_dict(s['output']['structure']).sites))
                    plot_epa_c.append(s['output']['energy_per_atom'])
                else:
                    di['bc'] = 'bulk'
                    plot_nat_b.append(len(Structure.from_dict(s['output']['structure']).sites))
                    plot_epa_b.append(s['output']['energy_per_atom'])
                    plot_vpa_b.append(Structure.from_dict(s['output']['structure']).volume/len(Structure.from_dict(s['output']['structure']).sites))

                training_set.append(di)
                di = {}
            if len(s['calcs_reversed'][0]['output']['ionic_steps']) == 1:
                continue

            maxmin_force = [[5.00,4.50],[4.50,4.00],[4.00,3.50],[3.50,3.00],\
                            [3.00,2.80],[2.80,2.60],[2.60,2.40],[2.40,2.20],[2.20,2.00],\
                            [2.00,1.80],[1.80,1.60],[1.60,1.40],[1.40,1.20],[1.20,1.00],\
                            [1.00,0.70],[0.70,0.40],[0.40,0.10],\
                            [0.10,0.09],[0.09,0.08],[0.08,0.07],[0.07,0.06],[0.06,0.05]]

            found = len(maxmin_force) * [False]

            for ionic_step in range(len(s['calcs_reversed'][0]['output']['ionic_steps'])-1, 0, -1):
                if not False in found:
                    break
                this_epot = float(s['calcs_reversed'][0]['output']['ionic_steps'][ionic_step]['e_wo_entrp'])
                nsites = s['calcs_reversed'][0]['nsites']
                this_epa = this_epot/nsites
                if this_epa < min_epa + e_window:
                    this_forces = s['calcs_reversed'][0]['output']['ionic_steps'][ionic_step]['forces']
                    this_tot_forces = []
                    for a_force in range(len(this_forces)):
                        this_tot_forces.append(math.sqrt(this_forces[a_force][0]**2 + this_forces[a_force][1]**2 + this_forces[a_force][2]**2))
                    max_this_tot_foce = max(this_tot_forces)
                    for f in range(len(maxmin_force)):
                        if not found[f] and max_this_tot_foce < maxmin_force[f][0] and max_this_tot_foce >= maxmin_force[f][1]:
                            di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][ionic_step]['structure']
                            di['forces'] = this_forces
                            di['energy'] = this_epot
                            if 'cluster' in s['task_label']:
                                di['bc'] = 'free'
                                plot_nat_c.append(len(Structure.from_dict(s['output']['structure']).sites))
                                plot_epa_c.append(s['output']['energy_per_atom'])
                            else:
                                di['bc'] = 'bulk'
                                plot_nat_b.append(len(Structure.from_dict(s['output']['structure']).sites))
                                plot_epa_b.append(s['output']['energy_per_atom'])
                                plot_vpa_b.append(Structure.from_dict(s['output']['structure']).volume/len(Structure.from_dict(s['output']['structure']).sites))

                            training_set.append(di)
                            di = {}
                            found[f] = True
                            break

    plot_1_train(step_number, plot_nat_b, plot_epa_b, plot_vpa_b, plot_nat_c, plot_epa_c)

    if s_n > 1:
        try:
            with open(os.path.join(p_step_path,'train','position_force_train_all.json'), 'r') as f:
                prev_structs =json.loads(f.read())
            for a_p_s in prev_structs:
                if float(a_p_s["energy"])/len(a_p_s["structure"]["sites"]) < min_epa + e_window:
                    training_set.append(a_p_s)
        except:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no training data from the previous step <<<'+'\n')
    with open(os.path.join(Flame_dir,step_number,'train','position_force_train_all.json'), 'w') as f:
        json.dump(training_set, f)
    with open(os.path.join(Flame_dir,step_number,'train','nat_epa.dat'), 'w') as f:
        for i in range(len(training_set)):
            nat = len(Structure.from_dict(training_set[i]['structure']).sites)
            epot = training_set[i]['energy']
            epa = epot/nat
            f.write('{} {}'.format(str(nat), str(epa))+'\n')

def store_symmetry_function(step_number):
    all_distances = []
    pool = mp.Pool(mp.cpu_count())

    result_objects_d = []

    r_c = r_cut()
    with open(os.path.join(Flame_dir,step_number,'train','position_force_train_all.json'), 'r') as f:
        p_f = json.loads(f.read())
    for a_p_f in p_f:
        result_objects_d.append(pool.apply_async(read_distances, args=(r_c, a_p_f)))
    for r_o in result_objects_d:
        distances  = r_o.get()
        all_distances.extend(distances)

    pool.close()
    pool.join()

    min_distance = min(all_distances)
    with open(os.path.join(Flame_dir,step_number,'train','min_distance.dat'), 'w') as f:
        f.writelines("%s\n" % min_distance)

    plot_3_train(step_number, all_distances)

    if input_list['user_specified_FLAME_files'] and os.path.exists(os.path.join(run_dir,'flame_files','ann_input.yaml')):
        shutil.copyfile(os.path.join(run_dir,'flame_files','ann_input.yaml'), os.path.join(Flame_dir,step_number,'train','ann_input.yaml'))
    else:
        min_distance_bohr = min_distance * 1.88973
        g02s = return_g02s(min_distance_bohr, 10)
        g05s = return_g05s(min_distance_bohr, 4)

        with open(os.path.join(Flame_dir,step_number,'train','ann_input.yaml'), 'w') as f:
            f.write('{}'.format('g02:')+'\n')
            for g02 in g02s:
                f.write('{} {:.4f} {:.4f} {:.4f} {:.4f}'.format('-', g02, 0, 0, 0)+'\n')
            f.write('{}'.format('g05:')+'\n')
            for g05 in g05s:
                f.write('{} {:.4f} {:.4f} {:.4f} {:.4f} {:.4f}'.format('-', g05[0], g05[1], g05[2], 0, 0)+'\n')

def return_g02s(min_distance_bohr, n_g02):
    g02s = []

    r_c_bohr = r_cut() * 1.88973
    fc_max_eta = (1-(min_distance_bohr/r_c_bohr)**2)**3

    max_eta_g02 = -1.0 * math.log(0.1/fc_max_eta)/(min_distance_bohr**2)
    min_eta_g02 = -1.0 * math.log(0.9)/((0.7*r_c_bohr)**2)

    function = lambda x: 0.5 - math.exp(-1*max_eta_g02*(x**2))*(1-(x/r_c_bohr)**2)**3
    x1_g02 = fsolve(function,2)[0]
    if x1_g02 < 0:
        x1_g02 = x1_g02 * -1
    function = lambda x: 0.5 - math.exp(-1*min_eta_g02*(x**2))*(1-(x/r_c_bohr)**2)**3
    x2_g02 = fsolve(function,1)[0]
    if x2_g02 < 0:
        x2_g02 = x2_g02 * -1

    x_steps = (x2_g02-x1_g02)/(n_g02-1)

    for i in range(n_g02):
        x_bohr = x1_g02+i*x_steps
        fc = (1-(x_bohr/r_c_bohr)**2)**3
        eta = round(-1.0 * math.log(0.5/fc)/(x_bohr)**2, 4)
        g02s.append(eta)
    return g02s

def return_g05s(min_distance_bohr, n_g05):
    g05s = []

    r_c_bohr = r_cut() * 1.88973
    fc_max_eta = (1-(min_distance_bohr/r_c_bohr)**2)**3

    max_eta_g05 = -1.0 * math.log(0.1/(fc_max_eta**2))/(2*(min_distance_bohr**2))
    min_eta_g05 = -1.0 * math.log(0.9)/((0.7*r_c_bohr)**2)

    function = lambda x: 0.5 - math.exp(-1*max_eta_g05*2*(x**2))*(1-(x/r_c_bohr)**2)**6
    x1_g05 = fsolve(function,2)[0]
    if x1_g05 < 0:
        x1_g05 = x1_g05 * -1
    function = lambda x: 0.5 - math.exp(-1*min_eta_g05*2*(x**2))*(1-(x/r_c_bohr)**2)**6
    x2_g05 = fsolve(function,1)[0]
    if x2_g05 < 0:
        x2_g05 = x2_g05 * -1


    x_steps = (x2_g05-x1_g05)/(n_g05-1)
    for i in range(n_g05):
        x_bohr = x1_g05+i*x_steps
        fc = (1-(x_bohr/r_c_bohr)**2)**3
        eta = round(-1.0 * math.log(0.5/(fc**2))/(2*(x_bohr**2)), 4)
        g05s.extend([[eta,1,1],[eta,1,-1],[eta,2,1],[eta,2,-1],[eta,4,1],[eta,4,-1],[eta,12,1],[eta,12,-1]])
    return g05s

def read_distances(r_c, a_p_f):
    distances = []

    structure = Structure.from_dict(a_p_f['structure'])
    d_matrix = structure.distance_matrix

    upper_indices = np.triu_indices_from(d_matrix, k=1)
    upper_distances = d_matrix[upper_indices]
    for a_distance in upper_distances:
        if a_distance < r_c:
            distances.append(a_distance)
    return distances

def plot_1_train(step_number, plot_nat_b, plot_epa_b, plot_vpa_b, plot_nat_c, plot_epa_c):
    with open(os.path.join(random_structures_dir,'min_epa.dat'), 'r') as f:
        min_epa = float(f.readline().strip())

    with open(os.path.join(random_structures_dir,'vpa.dat'), 'r') as f:
        vpas = [float(line.strip()) for line in f]

    if len(plot_nat_b) > 0 and len(plot_epa_b) > 0:
        plt.figure(1)
        plt.scatter(plot_nat_b,plot_epa_b, label='epa-vs-nat')
        plt.xlabel('nat')
        plt.ylabel(r'epa $eV/atom$')
        plt.plot([min(plot_nat_b), max(plot_nat_b)], [min_epa, min_epa])
        plt.savefig(os.path.join(Flame_dir,step_number,'train','bulk_epa-vs-nat.png'))
        plt.close()

    if len(plot_epa_b) > 0 and len(plot_vpa_b) > 0:
        plt.figure(2)
        plt.scatter(plot_vpa_b,plot_epa_b, label='epa-vs-vpa')
        plt.xlabel(r'vpa ${\AA}^3/atom$')
        plt.ylabel(r'epa $eV/atom$')
        plt.plot([min(vpas)*0.85, min(vpas)*0.85], [min(plot_epa_b), max(plot_epa_b)], linestyle='dashed', color='green')
        plt.plot([min(vpas), min(vpas)], [min(plot_epa_b), max(plot_epa_b)], color='green')
        plt.plot([max(vpas), max(vpas)], [min(plot_epa_b), max(plot_epa_b)], color='orange')
        plt.plot([max(vpas)*1.20, max(vpas)*1.20], [min(plot_epa_b), max(plot_epa_b)], linestyle='dashed', color='orange')
        plt.plot([min(plot_vpa_b), max(plot_vpa_b)], [min_epa, min_epa], color='navy')
        plt.savefig(os.path.join(Flame_dir,step_number,'train','bulk_epa-vs-vpa.png'))
        plt.close()

    if len(plot_nat_c) > 0 and len(plot_epa_c) > 0:
        plt.figure(3)
        plt.scatter(plot_nat_c,plot_epa_c, label='epa-vs-nat')
        plt.xlabel('nat')
        plt.ylabel(r'epa $eV/atom$')
        plt.plot([min(plot_nat_c), max(plot_nat_c)], [min_epa, min_epa], color='navy')
        plt.savefig(os.path.join(Flame_dir,step_number,'train','cluster_epa-vs-nat.png'))
        plt.close()

def plot_2_train(step_number):
    plot_epoch = []
    plot_train_rmse = []
    plot_train_frmse = []
    plot_valid_rmse = []
    plot_valid_frmse = []

    with open(os.path.join(Flame_dir,step_number,'train','train_output.yaml'), 'r') as f:
        t_o = yaml.load(f, Loader=yaml.FullLoader)
    for i in range(len(t_o['training iterations'])):
        plot_epoch.append(t_o['training iterations'][i]['train']['iter'])

        plot_train_rmse.append(t_o['training iterations'][i]['train']['rmse']*27.2114)
        plot_train_frmse.append(t_o['training iterations'][i]['train']['frmse']*51422.1)

        plot_valid_rmse.append(t_o['training iterations'][i]['valid']['rmse']*27.2114)
        plot_valid_frmse.append(t_o['training iterations'][i]['valid']['frmse']*51422.1)

    plt.figure(1)
    plt.plot(plot_epoch[1:],plot_train_rmse[1:], label='train_rmse')
    plt.plot(plot_epoch[1:],plot_valid_rmse[1:], label='valid_rmse')
    plt.xlabel('epoch')
    plt.ylabel(r'RMSE $meV/atom$')
    plt.legend()
    plt.savefig(os.path.join(Flame_dir,step_number,'train','rmse.png'))
    plt.close()

    plt.figure(2)
    plt.plot(plot_epoch[1:],plot_train_frmse[1:], label='train_frmse')
    plt.plot(plot_epoch[1:],plot_valid_frmse[1:], label='valid_frmse')
    plt.xlabel('epoch')
    plt.ylabel(r'RMSE $meV/\AA$')
    plt.legend()
    plt.savefig(os.path.join(Flame_dir,step_number,'train','frmse.png'))
    plt.close()

def plot_3_train(step_number, all_distances):
    min_d = min(all_distances)
    max_d = r_cut()
    d_step = (max_d - min_d)/20
    d_intervals = []
    for i in range(20):
        d_intervals.append([min_d+i*d_step,min_d+(i+1)*d_step])
    to_plot = defaultdict(int)
    all_d = 0
    for a_d in all_distances:
        if a_d < max_d:
            val = plot_3_read(a_d, d_intervals)
            if val != 0:
                to_plot[val] = to_plot[val] + 1
                all_d = all_d + 1
    plot_d = []
    plot_b = []
    plot_i = [i*4 for i in range(1,21)]
    for keys, values in sorted(to_plot.items()):
        plot_d.append(str(keys))
        plot_b.append(values/all_d)
    plt.figure(6)
    plt.bar(plot_i, plot_b, width = 4 * d_step)
    plt.xticks(plot_i, plot_d)
    plt.xticks(rotation='vertical')
    plt.savefig(os.path.join(Flame_dir,step_number,'train','bonds.png'))
    plt.close()

def plot_3_read(a_d, d_intervals):
    for d_i in d_intervals:
        if a_d > d_i[0] and a_d <= d_i[1]:
            return round((d_i[0]+d_i[1])/2, 2)
    return 0
