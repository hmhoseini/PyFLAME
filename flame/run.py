from custodian import Custodian
from fireworks import explicit_serialize, FiretaskBase
from atomate.utils.utils import env_chk, get_logger
from flame.jobs import FlameJob
from flame.validators import *
from flame.handlers import *

logger = get_logger(__name__)

@explicit_serialize
class RunFlameCustodian(FiretaskBase):
    required_params = ["flame_cmd"]
    optional_params = ["handler_group", "validator_group", "job_type"]

    def run_task(self, fw_spec):

        handler_groups = {
            "default": [FrozenFlameErrorHandler(timeout=21600)],
            "train_user_epoch": [FrozenFlameErrorHandler(timeout=21600)],
            "train_auto_epoch": [FrozenFlameErrorHandler(timeout=21600), IncrreasingRMSEHandler()],
            "minhocao": [IdleMinhocaoErrorHandler()],
            "minhopp":  [IdleMinhoppErrorHandler()],
            "divcheck": [FrozenFlameErrorHandler(timeout=14400)]
        }

        flame_cmd = env_chk(self["flame_cmd"], fw_spec)
        jobs = [FlameJob(flame_cmd = flame_cmd)]
        job_type = self.get("job_type", "normal")
        if job_type == "normal":
            handlers = handler_groups["default"]
        elif job_type == "train_user_epoch" or job_type == "train_auto_epoch" or job_type == "minhocao" or job_type == "minhopp" or job_type == "divcheck":
            handlers = handler_groups[job_type]
        else:
            raise ValueError(f"Unsupported job type: {job_type}")
        c = Custodian(handlers, jobs, validators=[FlameFilesValidator()])
        c.run()
