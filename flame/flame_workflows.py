import os
import json
import re
from random import sample
from pymatgen.core.structure import Structure
from pymatgen.core.composition import Composition
from fireworks import Workflow
from structure.fireworks import StoreRandomStructFW
from structure.core import is_structure_valid, get_allowed_n_atom_for_compositions
from flame.flame_fireworks import *
from workflows.config import *

__author__ = "Sai Ram Kuchana and Hossein Mirhosseini"
__maintainer__ = "Hossein Mirhosseini"
__email__ = "mirhosse@mail.uni-paderborn.de"

def get_gensymcrys_wf(composition_list):
    wf_name = 'step-1'
    gensymcrys_fws = []
    for a_comp in composition_list:
        elements = []
        n_elmnt = []
        for e, n in Composition(a_comp).items():
            elements.append(str(e))
            n_elmnt.append(int(n))
        allowed_n_atom = get_allowed_n_atom_for_compositions([a_comp])

        attempts = round((input_list['max_number_of_bulk_structures']/len(input_list['bulk_number_of_atoms']) +\
                     input_list['max_number_of_reference_structures']/len(input_list['reference_number_of_atoms']))*5/230)
        if attempts == 0:
            attempts = 1
        for a_n_a in allowed_n_atom:
            for i in range(attempts):
                gensymcrysfw = GensymcrysFW(elements, n_elmnt, a_n_a, name='gensymcrys_'+str(a_comp)+'_'+str(a_n_a)+'_atoms')
                gensymcrys_fws.append(gensymcrysfw)
    store_fw = [StoreRandomStructFW(name='store_random_structures', parents = gensymcrys_fws, spec={'_allow_fizzled_parents': True})]
    fws = []
    fws = gensymcrys_fws + store_fw
    gensymcrys_wf = Workflow(fws, name = wf_name)
    return gensymcrys_wf

def get_aver_dist_wf(composition_list):
    wf_name = 'aver_dist'
    aver_dist_fws = []
    allowed_n_atom = get_allowed_n_atom_for_compositions(composition_list)
    for a_n_a in input_list['bulk_number_of_atoms']:
        if a_n_a in allowed_n_atom:
            averdistfw = AverDistFW(a_n_a = a_n_a, name = 'aver_dist_'+str(a_n_a)+'_atoms')
            aver_dist_fws.append(averdistfw)
    aver_dist_wf = Workflow(aver_dist_fws, name = wf_name)
    return aver_dist_wf

def get_train_pre_wf(c_step_number, wf_name):
    train_pre_fw = [Train_pre_FW(step_number = c_step_number, name = 'flame_train_pre')]
    train_pre_wf = Workflow(train_pre_fw, name = wf_name)
    return train_pre_wf

def get_train_wf(c_step_number, wf_name):
    train_fws = []
    if input_list['number_of_epoch']:
        t_type = 'train_user_epoch'
    else:
        t_type = 'train_auto_epoch'
    for i in range(3):
        trainfw = TrainFW(step_number = c_step_number, name = 'flame_train', train_type = t_type)
        train_fws.append(trainfw)
    train_wf = Workflow(train_fws, name = wf_name)
    return train_wf

def get_train_post_wf(c_step_number, wf_name):
    train_post_fw = [Train_post_FW(step_number = c_step_number, name = 'flame_train_post')]
    train_post_wf = Workflow(train_post_fw, name = wf_name)
    return train_post_wf

def get_minhocao_wf(c_step_number, wf_name):
    minhocao_wf = []
    s_n = int(re.split('-',c_step_number)[1])

    if input_list['bulk_minhocao'][s_n - 1] != 0:
        min_d_prefactor = input_list['min_distance_prefactor'] * ((100-float(input_list['descending_prefactor']))/100)**(s_n - 1)\
                          if input_list['descending_prefactor'] else input_list['min_distance_prefactor']

        file_name = os.path.join(random_structures_dir,'minhocao_seeds.json')
        with open(file_name, 'r') as f:
            structs  = json.loads(f.read())
        for i in range(1, s_n):
            step_n = 'step-'+str(i)
            f_path = os.path.join(Flame_dir,step_n,'minhocao')
            if os.path.exists(os.path.join(f_path,'next-step_minhocao_seeds.json')):
                with open(os.path.join(f_path,'next-step_minhocao_seeds.json'), 'r') as f:
                    next_step_struct = json.loads(f.read())
                structs.extend(next_step_struct)
        valid_structs = []
        for a_strut in structs:
            if is_structure_valid(Structure.from_dict(a_strut), min_d_prefactor, False, False):
                valid_structs.append(a_strut)
        selected_structs = sample(valid_structs, input_list['bulk_minhocao'][s_n - 1]) if len(valid_structs) > input_list['bulk_minhocao'][s_n - 1] else valid_structs
        minhocao_fws = []
        for struct_number, struct in enumerate(selected_structs):
            nat = len(Structure.from_dict(struct).sites)
            if nat > 70:
                priority = 3
            elif nat > 40:
                priority = 2
            else:
                priority = 1
            minhocaofw = MinhocaoFW(structure = Structure.from_dict(struct), step_number = c_step_number, name = 'flame_minhocao_struct-'+str(struct_number+1), spec={'_priority': priority})
            minhocao_fws.append(minhocaofw)
        minhocao_fws_split = [minhocao_fws[i:i+100] for i in range(0, len(minhocao_fws), 100)]
        for i in range(len(minhocao_fws_split)):
            minhocao_wf.append(Workflow(minhocao_fws_split[i], name = wf_name))
    return minhocao_wf

def get_minhocao_store_wf(c_step_number, wf_name):
    minhocao_store_fw = []
    minhocaostorefw = MinhocaoStoreFW(step_number = c_step_number, name = 'minhocao_store-results')
    minhocao_store_fw.append(minhocaostorefw)
    minhocao_store_wf = Workflow(minhocao_store_fw, name = wf_name)
    return minhocao_store_wf

def get_minhopp_wf(c_step_number, wf_name):
    minhopp_b_wf = []
    minhopp_c_wf = []

    s_n = int(re.split('-',c_step_number)[1])
    min_d_prefactor = input_list['min_distance_prefactor'] * ((100-float(input_list['descending_prefactor']))/100)**(s_n - 1)\
                      if input_list['descending_prefactor'] else input_list['min_distance_prefactor']

    if input_list['cluster_minhopp'][s_n - 1] > 0:

        file_name = os.path.join(random_structures_dir,'minhopp_cluster_seeds.json')
        with open(file_name, 'r') as f:
            clusters = json.loads(f.read())
        for i in range(1, s_n):
            step_n = 'step-'+str(i)
            f_path = os.path.join(Flame_dir,step_n,'minhopp')
            if os.path.exists(os.path.join(f_path,'next-step_minhopp_cluster_seeds.json')):
                with open(os.path.join(f_path,'next-step_minhopp_cluster_seeds.json'), 'r') as f:
                    next_step_c = json.loads(f.read())
                clusters.extend(next_step_c)
        valid_clusters = []
        for a_cluster in clusters:
            if len(Structure.from_dict(a_cluster).sites) in input_list['cluster_number_of_atoms'] and\
               is_structure_valid(Structure.from_dict(a_cluster), min_d_prefactor, False, False):
                valid_clusters.append(a_cluster)
        selected_clusters = sample(valid_clusters, input_list['cluster_minhopp'][s_n - 1]) if len(clusters) > input_list['cluster_minhopp'][s_n - 1] else valid_clusters
        minhoppc_fws = []
        for struct_number, struct in enumerate(selected_clusters):
            nat = len(Structure.from_dict(struct).sites)
            if nat > 70:
                priority = 6
            elif nat > 40:
                priority = 5
            else:
                priority = 4
            minhoppcfw = MinhoppFW(structure = Structure.from_dict(struct), step_number = c_step_number, name = 'flame_minhopp_cluster-'+str(struct_number+1), job_type = 'cluster', spec={'_priority': priority})
            minhoppc_fws.append(minhoppcfw)
        if len(minhoppc_fws) > 0:
            minhoppc_fws_split = [minhoppc_fws[i:i+100] for i in range(0, len(minhoppc_fws), 100)]
            for i in range(len(minhoppc_fws_split)):
                minhopp_c_wf.append(Workflow(minhoppc_fws_split[i], name = wf_name))

    if input_list['bulk_minhopp'][s_n - 1] > 0:
        file_name = os.path.join(random_structures_dir,'minhopp_bulk_seeds.json')
        with open(file_name, 'r') as f:
            stressed_bulks = json.loads(f.read())
        for i in range(1, s_n):
            step_n = 'step-'+str(i)
            f_path = os.path.join(Flame_dir,step_n,'minhopp')
            if os.path.exists(os.path.join(f_path,'next-step_minhopp_bulk_seeds.json')):
                with open(os.path.join(f_path,'next-step_minhopp_bulk_seeds.json'), 'r') as f:
                    next_step_b = json.loads(f.read())
                stressed_bulks.extend(next_step_b)
        valid_stressed_bulks = []
        for a_stressed_bulk in stressed_bulks:
            if is_structure_valid(Structure.from_dict(a_stressed_bulk), min_d_prefactor, False, False):
                valid_stressed_bulks.append(a_stressed_bulk)
        selected_stressed_bulks = sample(valid_stressed_bulks, input_list['bulk_minhopp'][s_n - 1]) if len(stressed_bulks) > input_list['bulk_minhopp'][s_n - 1] else valid_stressed_bulks

        minhoppb_fws = []
        for struct_number, struct in enumerate(selected_stressed_bulks):
            nat = len(Structure.from_dict(struct).sites)
            if nat > 70:
                priority = 3
            elif nat > 40:
                priority = 2
            else:
                priority = 1
            minhoppbfw = MinhoppFW(structure = Structure.from_dict(struct), step_number = c_step_number, name = 'flame_minhopp_bulk-'+str(struct_number+1), job_type = 'bulk', spec={'_priority': priority})
            minhoppb_fws.append(minhoppbfw)
        if len(minhoppb_fws) > 0:
            minhoppb_fws_split = [minhoppb_fws[i:i+100] for i in range(0, len(minhoppb_fws), 100)]
            for i in range(len(minhoppb_fws_split)):
                minhopp_b_wf.append(Workflow(minhoppb_fws_split[i], name = wf_name))

    return minhopp_c_wf, minhopp_b_wf

def get_minhopp_store_wf(c_step_number, wf_name):
    minhopp_store_fw = []
    minhoppstorefw = MinhoppStoreFW(step_number = c_step_number, name = 'minhopp_store-results')
    minhopp_store_fw.append(minhoppstorefw)
    minhopp_store_wf = Workflow(minhopp_store_fw, name = wf_name)
    return minhopp_store_wf

def get_divcheck_b_wf(c_step_number, wf_name):
    with open(os.path.join(Flame_dir,'aver_dist','aver_dist.json'), 'r') as f:
        aver_dist_dict = yaml.load(f, Loader=yaml.FullLoader)
    lines =[]

    if os.path.exists(os.path.join(Flame_dir,c_step_number,'minhocao','nats.json')):
        with open(os.path.join(Flame_dir,c_step_number,'minhocao','nats.json'), 'r') as f:
            minhocao_nats = json.loads(f.read())
    else:
        minhocao_nats['bulk'] = []

    if os.path.exists(os.path.join(Flame_dir,c_step_number,'minhopp','nats.json')):
        with open(os.path.join(Flame_dir,c_step_number,'minhopp','nats.json'), 'r') as f:
            minhopp_nats = json.loads(f.read())
    else:
         minhopp_nats['bulk'] = []

    bulk_nats = minhocao_nats['bulk']
    for n in minhopp_nats['bulk']:
        if n not in minhocao_nats['bulk']:
            bulk_nats.append(n)

    divcheck_b_fws = []
    for nat in bulk_nats:
        dt = float(aver_dist_dict[nat]) * float(input_list['dtol_prefactor'])
        divcheckbfw = DivCheckbFW(number_of_atom = nat, dtol =  dt, step_number = c_step_number, name = 'flame_divcheck_'+nat+'-atom-bulk')
        divcheck_b_fws.append(divcheckbfw)
    divcheck_b_wf = []
    if len(divcheck_b_fws) > 0:
        divcheck_b_wf = Workflow(divcheck_b_fws, name = wf_name)
    return divcheck_b_wf

def get_divcheck_c_wf(c_step_number, wf_name):
    with open(os.path.join(Flame_dir,'aver_dist','aver_dist.json'), 'r') as f:
        aver_dist_dict = yaml.load(f, Loader=yaml.FullLoader)
    lines =[]
    if os.path.exists(os.path.join(Flame_dir,c_step_number,'minhopp','nats.json')):
        with open(os.path.join(Flame_dir,c_step_number,'minhopp','nats.json'), 'r') as f:
            minhopp_nats = json.loads(f.read())
    else:
        minhopp_nats['cluster'] = []

    cluster_nats = minhopp_nats['cluster']

    divcheck_c_fws = []
    for nat in cluster_nats:
        dt = float(aver_dist_dict[nat]) * float(input_list['dtol_prefactor']) * input_list['prefactor_cluster']
        divcheckcfw = DivCheckcFW(number_of_atom = nat, dtol =  dt, step_number = c_step_number, name = 'flame_divcheck_'+nat+'-atom-cluster')
        divcheck_c_fws.append(divcheckcfw)
    divcheck_c_wf = []
    if len(divcheck_c_fws) > 0:
        divcheck_c_wf = Workflow(divcheck_c_fws, name = wf_name)
    return divcheck_c_wf
