import os
import re
import json
import copy
from random import sample
from collections import defaultdict
import numpy as np
from pymatgen.core.structure import Structure, Molecule
from structure.core import read_element_list
from flame.core import write_p_f_from_list, write_SE_ann_input, write_FLAME_input_file
from flame.flame_functions.atoms import Atoms
from flame.flame_functions.io_yaml import read_yaml, write_yaml, atoms2dict
from workflows.config import *

def write_divcheck_b_files(step_number, nat):
    s_n = int(re.split('-',step_number)[1])
    poslow_structures = []
    energy_list = []
    if s_n > 1:
        for s_n_i in range(1, s_n):

            p_step_path = os.path.join(Flame_dir,'step-'+str(s_n_i),'minhocao')
            p_file_name = 'poslows-'+'step-'+str(s_n_i)+'.json'
            if os.path.exists(os.path.join(p_step_path,p_file_name)):
                with open(os.path.join(p_step_path,p_file_name), 'r') as f:
                    poslows_confs = json.loads(f.read())
                if nat in poslows_confs.keys():
                    for a_conf in poslows_confs[nat]:
                        lattice = a_conf['conf']['cell']
                        crdnts = []
                        spcs = []
                        for coord in a_conf['conf']['coord']:
                            crdnts.append([coord[0],coord[1],coord[2]])
                            spcs.append(coord[3])
                        poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                        energy_list.append(-1) # it is NOT a new structure
                else:
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: no poslow structure with {} atoms (generated with minhocao) from step-{} is available <<<'.format(nat, str(s_n_i))+'\n')
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no poslow structure with {} atoms (generated with minhocao) from step-{} is available <<<'.format(nat, str(s_n_i))+'\n')

            p_step_path = os.path.join(Flame_dir,'step-'+str(s_n_i),'minhopp')
            p_file_name = 'poslows-bulk-'+'step-'+str(s_n_i)+'.json'
            if os.path.exists(os.path.join(p_step_path,p_file_name)):
                with open(os.path.join(p_step_path,p_file_name), 'r') as f:
                    poslows_confs = json.loads(f.read())
                if nat in poslows_confs.keys():
                    for a_conf in poslows_confs[nat]:
                        lattice = a_conf['conf']['cell']
                        crdnts = []
                        spcs = []
                        for coord in a_conf['conf']['coord']:
                            crdnts.append([coord[0],coord[1],coord[2]])
                            spcs.append(coord[3])
                        poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                        energy_list.append(-1) # it is NOT a new structure
                else:
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: no poslow structure with {} atoms (generated with bulk minhopp) from step-{} is available <<<'.format(nat, str(s_n_i))+'\n')
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no poslow structure with {} atoms (generated with bulk minhopp) from step-{} is available <<<'.format(nat, str(s_n_i))+'\n')

    if len(poslow_structures) > 10000:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: number of poslow bulk structures with {} atoms from previous cycles is larger than 10000 <<<'.format(nat)+'\n')

    if os.path.exists(os.path.join(Flame_dir,step_number,'minhopp','poslows-bulk-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhopp','poslows-bulk-'+step_number+'.json'), 'r') as f:
            poslows_confs = json.loads(f.read())
        if nat in poslows_confs.keys():
            for a_conf in poslows_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                energy_list.append(1) # it is a new structure
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no poslow structure with {} atoms (generated with bulk minhopp) from step-{} s available <<<'.format(nat, str(step_number))+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no poslow structure with {} atoms (generated with bulk minhopp) from step-{} is available <<<'.format(nat, str(step_number))+'\n')

    if os.path.exists(os.path.join(Flame_dir,step_number,'minhocao','poslows-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhocao','poslows-'+step_number+'.json'), 'r') as f:
            poslows_confs = json.loads(f.read())
        if nat in poslows_confs.keys():
            for a_conf in poslows_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                energy_list.append(1) # it is a new structure
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no poslow structure with {} atoms (generated with minhocao) from step-{} is available <<<'.format(nat, str(step_number))+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no poslow structure with {} atoms (generated with minhocao) from step-{} is available <<<'.format(nat, str(step_number))+'\n')

    structure_list = []
    if os.path.exists(os.path.join(Flame_dir,step_number,'minhocao','minhocao-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhocao','minhocao-'+step_number+'.json'), 'r') as f:
            minhocao_confs = json.loads(f.read())
        if nat in minhocao_confs.keys():
            for a_conf in minhocao_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                structure_list.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no structure with {} atoms (generated with minhocao) from step-{} is available <<<'.format(nat, str(step_number))+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no structure with {} atoms (generated with minhocao) from step-{} is available <<<'.format(nat, str(step_number))+'\n')

    if os.path.exists(os.path.join(Flame_dir,step_number,'minhopp','minhopp-bulk-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhopp','minhopp-bulk-'+step_number+'.json'), 'r') as f:
            minhopp_bulk_confs = json.loads(f.read())
        if nat in minhopp_bulk_confs.keys():
            for a_conf in minhopp_bulk_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                structure_list.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
        elif nat in input_list['reference_number_of_atoms']:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no structure with {} atoms (generated with bulk minhopp) from step-{} is available <<<'.format(nat, str(step_number))+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no structure with {} atoms (generated with bulk minhopp) from step-{} is available <<<'.format(nat, str(step_number))+'\n')

    selected_poslow_structures = poslow_structures[:10000] if len(poslow_structures) > 10000 else poslow_structures
    selected_structures = sample(structure_list, 10000-len(selected_poslow_structures)) if len(structure_list) >= 10000-len(selected_poslow_structures) else structure_list
    all_structures = selected_poslow_structures + selected_structures
    energy_list = energy_list + (10000-len(selected_poslow_structures)) * [1]
    bc_list = len(all_structures) * ['bulk']
    write_p_f_from_list(all_structures, bc_list, energy_list, False, 'position_force_divcheck.yaml')

    elmnt_list = read_element_list()
    write_SE_ann_input(elmnt_list, step_number)
    write_FLAME_input_file('divcheck', elmnt_list)
    with open('list_posinp_check.yaml', 'w') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_divcheck.yaml'+'\n')
    with open('nat.dat', 'w') as f:
        f.write(nat)

def write_divcheck_c_files(step_number, nat):
    s_n = int(re.split('-',step_number)[1])
    poslow_structures = []
    energy_list = []
    if s_n > 1:
        for s_n_i in range(1, s_n):

            p_step_path = os.path.join(Flame_dir,'step-'+str(s_n_i),'minhopp')
            p_file_name = 'poslows-cluster-'+'step-'+str(s_n_i)+'.json'

            if os.path.exists(os.path.join(p_step_path,p_file_name)):
                with open(os.path.join(p_step_path,p_file_name), 'r') as f:
                    poslows_confs = json.loads(f.read())
                if nat in poslows_confs.keys():
                    for a_conf in poslows_confs[nat]:
                        lattice = a_conf['conf']['cell']
                        crdnts = []
                        spcs = []
                        for coord in a_conf['conf']['coord']:
                            crdnts.append([coord[0],coord[1],coord[2]])
                            spcs.append(coord[3])
                        poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                        energy_list.append(-1) # it is NOT a new structure
                else:
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: no poslow structure with {} atoms (generated with cluster minhopp) from step-{} is available <<<'.format(nat, str(s_n_i))+'\n')
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no poslow structure with {} atoms (generated with cluster minhopp) from step-{} is available <<<'.format(nat, str(s_n_i))+'\n')

    if len(poslow_structures) > 10000:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: number of cluster poslow structures with {} atoms from previous cycles is larger than 10000 <<<'.format(nat)+'\n')

    if os.path.exists(os.path.join(Flame_dir,step_number,'minhopp','poslows-cluster-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhopp','poslows-cluster-'+step_number+'.json'), 'r') as f:
            poslow_confs = json.loads(f.read())
        if nat in poslow_confs.keys():
            for a_conf in poslow_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                energy_list.append(1) # it is a new structure
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no poslow structure with {} atoms (generated with cluster minhopp) from step-{} is available <<<'.format(nat, str(step_number))+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no poslow structure with {} atoms (generated with cluster minhopp) from step-{} is available <<<'.format(nat, str(step_number))+'\n')

    structure_list = []
    if os.path.exists(os.path.join(Flame_dir,step_number,'minhopp','minhopp-cluster-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhopp','minhopp-cluster-'+step_number+'.json'), 'r') as f:
            minhocao_confs = json.loads(f.read())
        if nat in minhocao_confs.keys():
            for a_conf in minhocao_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                structure_list.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no cluster structure with {} atoms from step-{} is is available <<<'.format(nat, str(step_number))+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no cluster structure with {} atoms from step-{} is available <<<'.format(nat, str(step_number))+'\n')

    selected_poslow_structures = poslow_structures[:10000] if len(poslow_structures) > 10000 else poslow_structures
    selected_structures = sample(structure_list, 10000-len(selected_poslow_structures)) if len(structure_list) >= 10000-len(selected_poslow_structures) else structure_list
    all_structures = selected_poslow_structures + selected_structures
    energy_list = energy_list + (10000-len(selected_poslow_structures)) * [1]
    bc_list = len(all_structures) * ['free']

    write_p_f_from_list(all_structures, bc_list, energy_list, False, 'position_force_divcheck.yaml')

    elmnt_list = read_element_list()
    write_SE_ann_input(elmnt_list, step_number)
    write_FLAME_input_file('divcheck', elmnt_list)
    with open('list_posinp_check.yaml', 'w') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_divcheck.yaml'+'\n')
    with open('nat.dat', 'w') as f:
        f.write(nat)

def run_pickdiff(nat, dtol):
    if dtol == 0:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no dtol is provided for structures with {} atoms <<<'.format(nat)+'\n')
    else:
        if os.path.exists('distall'):
            dist=[]
            f=open('distall','r')
            nn=-1
            nconf=0
            for line in f.readlines():
                if int(line.split()[1])!=nn:
                    nconf+=1
                    dist.append([])
                    for iconf in range(nconf-1):
                        dist[-1].append(dist[iconf][nconf-1])
                    dist[-1].append(0.0)
                    nn=int(line.split()[1])
                dist[-1].append(float(line.split()[4]))
            f.close()
            nconf+=1
            dist.append([])
            for iconf in range(nconf-1):
                dist[-1].append(dist[iconf][nconf-1])
            dist[-1].append(0.0)

            sel=[]
            for iconf in range(nconf):
                if iconf==0:
                    sel.append(0)
                    continue
                new=True
                for jconf in range(len(sel)):
                    if dist[iconf][sel[jconf]]<dtol:
                        new=False
                        break
                if new is True:
                    sel.append(iconf)
            atoms_all=read_yaml('position_force_divcheck.yaml')
            atoms_all_out=[]
            for iconf in range(len(sel)):
                atoms_all_out.append(Atoms)
                atoms_all_out[-1]=copy.deepcopy(atoms_all[sel[iconf]])
            atoms_all_yaml = []
            json_dump = []
            for atoms in atoms_all_out:
                dict_atoms=atoms2dict(atoms)
                if dict_atoms['conf']['epot'] > 0: # only new structures
                    atoms_all_yaml.append(atoms)
                    json_dump.append(dict_atoms)
            with open('checked_position_force.json', 'w') as f:
                json.dump(json_dump, f)

            write_yaml(atoms_all_yaml, 'checked_position_force.yaml')
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no distall in {} <<<'.format(os.getcwd())+'\n')

def collect_divcheck_results(step_number):
    SP_bulk_structures = defaultdict(list)
    SP_clusters = defaultdict(list)

    for a_file in os.listdir(os.path.join(Flame_dir,step_number,'divcheck')):
        nat = a_file.split('_')[0].split('-')[0]
        with open(os.path.join(Flame_dir,step_number,'divcheck',a_file) ,'r') as f:
            confs = json.loads(f.read())
        for conf in confs:
            lattice = conf['conf']['cell']
            crdnts = []
            spcs = []
            for coord in conf['conf']['coord']:
                crdnts.append([coord[0],coord[1],coord[2]])
                spcs.append(coord[3])
            if 'bulk' in a_file:
                struct = Structure(lattice, spcs, crdnts, coords_are_cartesian = True)
                SP_bulk_structures[nat].append(struct)
            if 'cluster' in a_file and input_list['cluster_calculation']:
                array_crdnts = np.array(crdnts)
                maxx = max(array_crdnts[:,0:1])[0]
                minx = min(array_crdnts[:,0:1])[0]
                maxy = max(array_crdnts[:,1:2])[0]
                miny = min(array_crdnts[:,1:2])[0]
                maxz = max(array_crdnts[:,2:3])[0]
                minz = min(array_crdnts[:,2:3])[0]
                a_cluster = maxx-minx+input_list['vacuum_length']
                b_cluster = maxy-miny+input_list['vacuum_length']
                c_cluster = maxz-minz+input_list['vacuum_length']
                if a_cluster > input_list['box_size'] or b_cluster > input_list['box_size'] or c_cluster > input_list['box_size']:
                    continue
                molecule = Molecule(spcs, crdnts)
                try:
                    boxed_molecule = molecule.get_boxed_structure(a_cluster,b_cluster,c_cluster)
                except:
                    continue
                SP_clusters[nat].append(boxed_molecule)
    return SP_bulk_structures, SP_clusters
