import os
import shutil
import re
import json
import copy
import matplotlib.pyplot as plt
from datetime import datetime
from random import sample
from collections import defaultdict
from pymatgen.core.structure import Structure
from pymatgen.io.vasp import Poscar
from structure.core import read_element_list, read_composition_list, is_structure_valid
from workflows.core import find_block_folder
from flame.core import write_FLAME_input_file
from flame.flame_functions.atoms import Atoms
from flame.flame_functions.io_yaml import write_yaml, read_yaml, atoms2dict
from flame.flame_functions.io_bin import bin_read
from flame.flame_functions.vasp import poscar_read
from workflows.config import *

def write_minhopp_files(struct, step_number, job_type):
    elmnt_list = read_element_list()

    Poscar(struct).write_file('posinp.vasp')
    atoms=poscar_read('posinp.vasp')
    atoms.units_length_io='angstrom'
    atoms.boundcond = 'bulk' if job_type == 'bulk' else 'free'
    atoms_out=[]
    atoms_out.append(Atoms())
    atoms_out[-1]=copy.copy(atoms)
    write_yaml(atoms_out,'posinp.yaml')

    composition_list = read_composition_list()
    write_FLAME_input_file('minhopp', elmnt_list, structure=struct, step_number=step_number, composition_list=composition_list, min_d_prefactor = input_list['min_distance_prefactor'])

    with open('input.minhopp', 'w') as f:
        f.write('             0 number of minima already found'+'\n')
        f.write('   0.01    0.001  0.1      ediff,ekin,dt'+'\n')

    for elmnt in elmnt_list:
        shutil.copyfile(os.path.join(Flame_dir,step_number,'train',str(elmnt)+'.ann.param.yaml'),\
                        os.path.join('./',str(elmnt)+'.ann.param.yaml'))

def store_minhopp_results(step_number):
    all_poslow_bstructs, all_poslow_cstructs, all_traj_bstructs, all_traj_cstructs, failed_bstructs, failed_cstructs = read_minhopp_results(step_number)

    with open(os.path.join(Flame_dir,step_number,'minhopp','poslows-bulk-'+step_number+'.json'), 'w') as f:
        json.dump(all_poslow_bstructs, f)
    with open(os.path.join(Flame_dir,step_number,'minhopp','poslows-cluster-'+step_number+'.json'), 'w') as f:
        json.dump(all_poslow_cstructs, f)
    with open(os.path.join(Flame_dir,step_number,'minhopp','minhopp-bulk-'+step_number+'.json'), 'w') as f:
        json.dump(all_traj_bstructs, f)
    with open(os.path.join(Flame_dir,step_number,'minhopp','minhopp-cluster-'+step_number+'.json'), 'w') as f:
        json.dump(all_traj_cstructs, f)
    with open(os.path.join(Flame_dir,step_number,'minhopp','failed_bulk_structures.json'), 'w') as f:
        json.dump(failed_bstructs, f)
    with open(os.path.join(Flame_dir,step_number,'minhopp','failed_cluster_structures.json'), 'w') as f:
        json.dump(failed_cstructs, f)
    keys = {}
    keys['bulk'] = [str(a_key) for a_key in all_poslow_bstructs.keys()]
    keys['cluster'] = [str(a_key) for a_key in all_poslow_cstructs.keys()]
    with open(os.path.join(Flame_dir,step_number,'minhopp','nats.json'), 'w') as f:
        json.dump(keys, f)

def read_minhopp_results(step_number):
    s_n = int(re.split('-',step_number)[1])
    min_d_prefactor = input_list['min_distance_prefactor'] * ((100-float(input_list['descending_prefactor']))/100)**(s_n)\
                      if input_list['descending_prefactor'] else input_list['min_distance_prefactor']

    with open(os.path.join(random_structures_dir,'min_epa.dat'), 'r') as f:
        min_epa = float(f.readline().strip())

    block_dir = find_block_folder()[0]

    all_poslow_bstructs = defaultdict(list)
    all_poslow_cstructs = defaultdict(list)
    all_traj_bstructs = defaultdict(list)
    all_traj_cstructs = defaultdict(list)

    bin_file_paths_all = []

    plot_nat_b = []
    plot_epa_b = []
    plot_vpa_b = []

    plot_nat_c = []
    plot_epa_c = []

    failed_bstructs = []
    failed_cstructs = []

    for root, dirs, files in os.walk(block_dir):
        if 'posinp.yaml' in files:
            if 'poslow.yaml' in files:
                bin_file_paths = []
                for a_file in files:
                    if 'mde' in a_file and a_file.endswith('bin'):
                        bin_file_paths.append(os.path.join(root,a_file))
                bin_file_paths_select = sample(bin_file_paths, 2)
                bin_file_paths_all.extend(bin_file_paths_select)
                with open('monitoring.dat', 'a') as f:
                    f.write('time: {}'.format(datetime.now().strftime("%H:%M:%S"))+'\n')
                    f.write('reading {}'.format(os.path.join(root,'poslow.yaml'))+'\n')
                try:
                    atoms_all_yaml = read_yaml(os.path.join(root,'poslow.yaml'))
                except:
                    continue
                for atoms in atoms_all_yaml:
                    conf = atoms2dict(atoms)
                    epot = conf['conf']['epot']
                    lattice = conf['conf']['cell']
                    crdnts = []
                    spcs = []
                    for coord in conf['conf']['coord']:
                        crdnts.append([coord[0],coord[1],coord[2]])
                        spcs.append(coord[3])
                    structure = Structure(lattice,spcs,crdnts,coords_are_cartesian=True)
                    nat = len(structure.sites)

                    if is_structure_valid(structure, min_d_prefactor, False, False):
                        if conf['conf']['bc'] == 'bulk':
                            all_poslow_bstructs[nat].append(conf)

                            volume = structure.volume
                            vpa = volume/nat
                            epa = 27.2114 * epot/nat
                            plot_nat_b.append(nat)
                            plot_epa_b.append(epa)
                            plot_vpa_b.append(vpa)

                            if epa < min_epa:
                                failed_bstructs.append(Structure.from_file(os.path.join(root,'posinp.vasp')).as_dict())

                        if conf['conf']['bc'] == 'free':

                            epa = 27.2114 * epot/nat
                            plot_nat_c.append(nat)
                            plot_epa_c.append(epa)

                            if epa < min_epa:
                                failed_cstructs.append(Structure.from_file(os.path.join(root,'posinp.vasp')).as_dict())
                            else:
                                all_poslow_cstructs[nat].append(conf)

            else:
                atoms_yaml = read_yaml(os.path.join(root,'posinp.yaml'))
                if atoms2dict(atoms_yaml[0])['conf']['bc'] == 'bulk':
                    failed_bstructs.append(Structure.from_file(os.path.join(root,'posinp.vasp')).as_dict())
                if atoms2dict(atoms_yaml[0])['conf']['bc'] == 'free':
                    failed_cstructs.append(Structure.from_file(os.path.join(root,'posinp.vasp')).as_dict())

                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no poslow.yaml file is found in {} <<<'.format(root)+'\n')

    for a_path in bin_file_paths_all:
        confs, bc, nat = read_bin_file(a_path, min_d_prefactor)
        if bc:
            if bc == 'bulk':
                all_traj_bstructs[nat].extend(confs)
            if bc == 'free':
                all_traj_cstructs[nat].extend(confs)

    with open(log_file, 'a') as f:
        f.write('time: {}'.format(datetime.now().strftime("%H:%M:%S"))+'\n')
        f.write('plotting'+'\n')

    plot_minhopp(step_number, plot_nat_b, plot_epa_b, plot_vpa_b, plot_nat_c, plot_epa_c)

    return all_poslow_bstructs, all_poslow_cstructs, all_traj_bstructs, all_traj_cstructs, failed_bstructs, failed_cstructs

def read_bin_file(path, min_d_prefactor):
    bohr2ang=0.52917721
    traj_structs = []
    atoms_all_bin = []

    with open('monitoring.dat', 'a') as f:
        f.write('time: {}'.format(datetime.now().strftime("%H:%M:%S"))+'\n')
        f.write('reading {}'.format(path)+'\n')
    try:
        atoms_all_bin = bin_read(path)
    except:
        return None, None, None

    if len(atoms_all_bin) < 1:
        return None, None, None

    atoms_bin_selected = sample(atoms_all_bin, 1)

    bc =  atoms_bin_selected[0].boundcond
    nat = atoms_bin_selected[0].nat
    for atoms in atoms_bin_selected:
        atoms.cellvec[0][0]=round(atoms.cellvec[0][0]*bohr2ang, 10)
        atoms.cellvec[0][1]=round(atoms.cellvec[0][1]*bohr2ang, 10)
        atoms.cellvec[0][2]=round(atoms.cellvec[0][2]*bohr2ang, 10)
        atoms.cellvec[1][0]=round(atoms.cellvec[1][0]*bohr2ang, 10)
        atoms.cellvec[1][1]=round(atoms.cellvec[1][1]*bohr2ang, 10)
        atoms.cellvec[1][2]=round(atoms.cellvec[1][2]*bohr2ang, 10)
        atoms.cellvec[2][0]=round(atoms.cellvec[2][0]*bohr2ang, 10)
        atoms.cellvec[2][1]=round(atoms.cellvec[2][1]*bohr2ang, 10)
        atoms.cellvec[2][2]=round(atoms.cellvec[2][2]*bohr2ang, 10)
        for iat in range(atoms.nat):
            atoms.rat[iat][0]=atoms.rat[iat][0]*bohr2ang
            atoms.rat[iat][1]=atoms.rat[iat][1]*bohr2ang
            atoms.rat[iat][2]=atoms.rat[iat][2]*bohr2ang
        conf=atoms2dict(atoms)
        lattice = conf['conf']['cell']
        crdnts = []
        spcs = []
        for coord in conf['conf']['coord']:
            crdnts.append([coord[0],coord[1],coord[2]])
            spcs.append(coord[3])
        structure = Structure(lattice, spcs, crdnts, coords_are_cartesian = True)
        if is_structure_valid(structure, min_d_prefactor, False, False):
            traj_structs.append(conf)
    return traj_structs, bc, nat

def plot_minhopp(step_number, plot_nat_b, plot_epa_b, plot_vpa_b, plot_nat_c, plot_epa_c):
    with open(os.path.join(random_structures_dir,'min_epa.dat'), 'r') as f:
        min_epa = float(f.readline().strip())

    with open(os.path.join(random_structures_dir,'vpa.dat'), 'r') as f:
        vpas = [float(line.strip()) for line in f]

    if len(plot_nat_b) > 0 and len(plot_epa_b) > 0:
        plt.figure(1)
        plt.scatter(plot_nat_b,plot_epa_b, label='epa-vs-nat')
        plt.xlabel('nat')
        plt.ylabel(r'epa $eV/atom$')
        plt.plot([min(plot_nat_b), max(plot_nat_b)], [min_epa, min_epa])
        plt.savefig(os.path.join(Flame_dir,step_number,'minhopp','minhopp_bulk_epa-vs-nat.png'))
        plt.close()

        plt.figure(2)
        plt.scatter(plot_vpa_b,plot_epa_b, label='epa-vs-vpa')
        plt.xlabel(r'vpa ${\AA}^3/atom$')
        plt.ylabel(r'epa $eV/atom$')
        plt.plot([min(vpas)*0.85, min(vpas)*0.85], [min(plot_epa_b), max(plot_epa_b)], linestyle='dashed', color='green')
        plt.plot([min(vpas), min(vpas)], [min(plot_epa_b), max(plot_epa_b)], color='green')
        plt.plot([max(vpas), max(vpas)], [min(plot_epa_b), max(plot_epa_b)], color='orange')
        plt.plot([max(vpas)*1.20, max(vpas)*1.20], [min(plot_epa_b), max(plot_epa_b)], linestyle='dashed', color='orange')
        plt.plot([min(plot_vpa_b), max(plot_vpa_b)], [min_epa, min_epa], color='navy')
        plt.savefig(os.path.join(Flame_dir,step_number,'minhopp','minhopp_bulk_epa-vs-vpa.png'))
        plt.close()

    if len(plot_nat_c) > 0 and len(plot_epa_c) > 0:
        plt.figure(3)
        plt.scatter(plot_nat_c,plot_epa_c, label='epa-vs-nat')
        plt.xlabel('nat')
        plt.ylabel(r'epa $eV/atom$')
        plt.plot([min(plot_nat_c), max(plot_nat_c)], [min_epa, min_epa])
        plt.savefig(os.path.join(Flame_dir,step_number,'minhopp','minhopp_cluster_epa-vs-nat.png'))
        plt.close()
