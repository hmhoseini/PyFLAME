import os
import shutil
import re
import json
from random import randint
import matplotlib.pyplot as plt
from datetime import datetime
from random import sample
from collections import defaultdict
from pymatgen.core.structure import Structure
from pymatgen.io.vasp import Poscar
from structure.core import is_structure_valid, read_element_list, read_composition_list
from workflows.core import find_block_folder
from flame.core import write_FLAME_input_file
from flame.flame_functions.atoms import Atoms
from flame.flame_functions.ascii import ascii_write, ascii_read
from flame.flame_functions.io_yaml import atoms2dict
from flame.flame_functions.vasp import poscar_read
from flame.flame_functions.latvec2dproj import latvec2dproj
from workflows.config import *

def write_minhocao_files(struct, step_number):
    elmnt_list = read_element_list()
    Poscar(struct).write_file('poscur.vasp')
    atoms=poscar_read('poscur.vasp')
    atoms.cellvec,atoms.rat=latvec2dproj(atoms.cellvec,atoms.rat,atoms.nat)
    ascii_write(atoms, 'poscur.ascii')

    composition_list = read_composition_list()

    write_FLAME_input_file('minhocao', elmnt_list, structure=struct, step_number=step_number, composition_list=composition_list, min_d_prefactor = input_list['min_distance_prefactor'])

    s_n = int(re.split('-',step_number)[1])
    minhocao_temp = randint(500*s_n, 1000*2**(s_n - 1))
    with open('ioput', 'w') as f:
        f.write('  0.01        {}       {}         ediff, temperature, maximal temperature'\
              .format(minhocao_temp, minhocao_temp*2)+'\n')

    with open('earr.dat', 'w') as f:
        f.write('  0         {}          # No. of minima already found, no. of minima to be found in consecutive run'\
               .format(input_list['minhocao_steps'][s_n - 1])+'\n')
        f.write('  0.400000E-03  0.150000E+00  # delta_enthalpy, delta_fingerprint')

    for elmnt in elmnt_list:
        shutil.copyfile(os.path.join(Flame_dir,step_number,'train',str(elmnt)+'.ann.param.yaml'),\
                        os.path.join('./',str(elmnt)+'.ann.param.yaml'))

def store_minhocao_results(step_number):
    all_poslow_structs, all_minhocao_structs, failed_structs = read_minhocao_results(step_number)

    with open(os.path.join(Flame_dir,step_number,'minhocao','poslows-'+step_number+'.json'), 'w') as f:
        json.dump(all_poslow_structs, f)
    with open(os.path.join(Flame_dir,step_number,'minhocao','minhocao-'+step_number+'.json'), 'w') as f:
        json.dump(all_minhocao_structs, f)

    with open(os.path.join(Flame_dir,step_number,'minhocao','failed_structures.json'), 'w') as f:
        json.dump(failed_structs, f)
    keys = {}
    keys['bulk'] = [str(a_key) for a_key in all_poslow_structs.keys()]
    with open(os.path.join(Flame_dir,step_number,'minhocao','nats.json'), 'w') as f:
        json.dump(keys, f)

def read_minhocao_results(step_number):
    s_n = int(re.split('-',step_number)[1])
    min_d_prefactor = input_list['min_distance_prefactor'] * ((100-float(input_list['descending_prefactor']))/100)**(s_n)\
                      if input_list['descending_prefactor'] else input_list['min_distance_prefactor']

    with open(os.path.join(random_structures_dir,'min_epa.dat'), 'r') as f:
        min_epa = float(f.readline().strip())

    with open(os.path.join(random_structures_dir,'vpa.dat'), 'r') as f:
        vpas = [float(line.strip()) for line in f]

    block_dir = find_block_folder()[0]

    all_poslow_structs = defaultdict(list)
    all_minhocao_structs = defaultdict(list)

    posmd_file_paths_all = []

    plot_nat = []
    plot_epa = []
    plot_vpa = []

    failed_structs = []

    for root, dirs in walklevel(block_dir, 1):
        for dirname in dirs:
            if os.path.exists(os.path.join(root,dirname,'poscur.ascii')):
                if os.path.exists(os.path.join(root,dirname,'global.mon')):
                    for a_file in os.listdir(os.path.join(root,dirname)):
                        if 'poslow' in a_file and a_file.endswith('ascii'):
                            with open('monitoring.dat', 'a') as f:
                                f.write('time: {}'.format(datetime.now().strftime("%H:%M:%S"))+'\n')
                                f.write('reading {}'.format(os.path.join(root,dirname,a_file))+'\n')
                            try:
                                atoms = ascii_read(os.path.join(root,dirname,a_file))
                            except:
                                continue
                            conf=atoms2dict(atoms)
                            if conf['conf']['coord'] or conf['conf']['nat'] != 0:
                                epot = conf['conf']['epot']
                                lattice = conf['conf']['cell']
                                crdnts = []
                                spcs = []
                                for coord in conf['conf']['coord']:
                                    crdnts.append([coord[0],coord[1],coord[2]])
                                    spcs.append(coord[3])
                                structure = Structure(lattice,spcs,crdnts,coords_are_cartesian=True)
                                nat = len(structure.sites)
                                volume = structure.volume
                                vpa = volume/nat
                                if is_structure_valid(structure, min_d_prefactor, True, False) and vpa <= vpas[1]*2:
                                    all_poslow_structs[nat].append(conf)

                                    epa = 27.2114 * epot/nat
                                    plot_nat.append(nat)
                                    plot_epa.append(epa)
                                    plot_vpa.append(vpa)

                                    if epa < min_epa:
                                        failed_structs.append(structure.as_dict())

                    with open(os.path.join(root,dirname,'global.mon'), 'r') as f:
                        lines = f.readlines()[1:]

                    folder_paths = []
                    for l in lines:
                        f_i = str(re.split('\s+',l)[1]).zfill(5)
                        folder_paths.append(os.path.join(root,dirname,'data_hop_'+f_i))
                    for a_folderpath in folder_paths:
                        posmd_file_paths = []
                        for a_file in os.listdir(a_folderpath):
                            if 'posmd' in a_file and a_file.endswith('ascii'):
                                posmd_file_paths.append(os.path.join(a_folderpath,a_file))

                        if len(posmd_file_paths) < 1:
                            continue
                        posmd_file_paths_selected = sample(posmd_file_paths, 1)
                        posmd_file_paths_all.extend(posmd_file_paths_selected)
                else:
                    failed_structs.append(Structure.from_file(os.path.join(root,dirname,'poscur.vasp')).as_dict())
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: no global.mon file is found in {} <<<'.format(os.path.join(root,dirname))+'\n')

    for a_path in posmd_file_paths_all:
        conf, nat = read_ascii_file(a_path, min_d_prefactor, vpas)
        if conf:
            all_minhocao_structs[nat].append(conf)

    with open(log_file, 'a') as f:
        f.write('time: {}'.format(datetime.now().strftime("%H:%M:%S"))+'\n')
        f.write('plotting'+'\n')

    plot_minhocao(step_number, plot_nat, plot_epa, plot_vpa)

    return all_poslow_structs, all_minhocao_structs, failed_structs

def read_ascii_file(path, min_d_prefactor, vpas):
    with open('monitoring.dat', 'a') as f:
        f.write('time: {}'.format(datetime.now().strftime("%H:%M:%S"))+'\n')
        f.write('reading {}'.format(path)+'\n')
    try:
        atoms=ascii_read(path)
        conf=atoms2dict(atoms)
    except:
        return None, None
    lattice = conf['conf']['cell']
    crdnts = []
    spcs = []
    for coord in conf['conf']['coord']:
        crdnts.append([coord[0],coord[1],coord[2]])
        spcs.append(coord[3])
    structure = Structure(lattice, spcs, crdnts, coords_are_cartesian=True)
    nat = len(structure.sites)
    volume = structure.volume
    vpa = volume/nat

    if is_structure_valid(structure, min_d_prefactor, True, False) and vpa <= vpas[1]*2:
        return conf, nat
    return None, None

def walklevel(path, depth):
    base_depth = path.rstrip(os.path.sep).count(os.path.sep)
    for root, dirs, files in os.walk(path):
        yield root, dirs
        cur_depth = root.count(os.path.sep)
        if base_depth + depth <= cur_depth:
            del dirs[:]

def plot_minhocao(step_number, plot_nat, plot_epa, plot_vpa):
    with open(os.path.join(random_structures_dir,'min_epa.dat'), 'r') as f:
        min_epa = float(f.readline().strip())
    with open(os.path.join(random_structures_dir,'vpa.dat'), 'r') as f:
        vpas = [float(line.strip()) for line in f]

    plt.figure(1)
    plt.scatter(plot_nat,plot_epa, label='epa-vs-nat')
    plt.xlabel('nat')
    plt.ylabel(r'epa $eV/atom')
    plt.plot([min(plot_nat), max(plot_nat)], [min_epa, min_epa])
    plt.savefig(os.path.join(Flame_dir,step_number,'minhocao','minhocao_epa-vs-nat.png'))
    plt.close()

    plt.figure(2)
    plt.scatter(plot_vpa,plot_epa, label='epa-vs-vpa')
    plt.xlabel(r'vpa ${\AA}^3/atom$')
    plt.ylabel(r'epa $eV/atom$')
    plt.plot([min(vpas)*0.85, min(vpas)*0.85], [min(plot_epa), max(plot_epa)], linestyle='dashed', color='green')
    plt.plot([min(vpas), min(vpas)], [min(plot_epa), max(plot_epa)], color='green')
    plt.plot([max(vpas), max(vpas)], [min(plot_epa), max(plot_epa)], color='orange')
    plt.plot([max(vpas)*1.20, max(vpas)*1.20], [min(plot_epa), max(plot_epa)], linestyle='dashed', color='orange')
    plt.plot([min(plot_vpa), max(plot_vpa)], [min_epa, min_epa], color='navy')
    plt.savefig(os.path.join(Flame_dir,step_number,'minhocao','minhocao_epa-vs-vpa.png'))
    plt.close()
