import os
import yaml
from time import sleep
import shutil
import subprocess
from fireworks.core.fworker import FWorker
from fireworks.core.launchpad import LaunchPad
from fireworks.queue.queue_launcher import rapidfire
from fireworks.user_objects.queue_adapters.common_adapter import CommonAdapter
from fireworks.utilities.fw_serializers import load_object_from_file
from workflows.config import *

launchpad = LaunchPad.from_file(os.path.join(config_dir,'my_launchpad.yaml'))

def launchpad_reset():
    try:
        launchpad.reset('', require_password = False, max_reset_wo_password = 10000)
        return True
    except:
        return False

def add_wf(wf):
    launchpad.add_wf(wf)

def run_jobs(job_type, l_dir):
    qadapter = set_qadapter(job_type)
    if not qadapter:
        return False
    try:
        submit_jobs(job_type, qadapter, l_dir)
        return True
    except:
        return False

def set_qadapter(job_type):
    qadapter = load_object_from_file(os.path.join(config_dir,'my_qadapter.yaml'))

    qadapter['job_name'] = config_list['job_name'] if config_list['job_name'] else 'pyflame'

    if job_type == 'geopt' or job_type == 'nsw0':
        if config_list['VASP_template']:
            qadapter.template_file = os.path.join(config_dir, config_list['VASP_template'])
        else:
            return False
    else:
        if config_list['FLAME_template']:
            qadapter.template_file = os.path.join(config_dir, config_list['FLAME_template'])
        else:
            return False

    job_script = config_list['job_script']

    if not job_script['default']['ntasks-per-node']:
        return False
    if job_script['default']['account']:
        qadapter['account'] = job_script['default']['account']
    if job_script['default']['partition']:
        qadapter['queue'] = job_script['default']['partition']
    if job_script['default']['time']:
        qadapter['time'] = str(job_script['default']['time'])

    if job_type == 'aver_dist':
        job_type = 'divcheck'
        qadapter['nodes'] = job_script[job_type]['nodes']
        qadapter['ntasks'] = job_script[job_type]['ntasks']
    elif job_type == 'train_pre':
        job_type = 'train'
        qadapter['nodes'] = 1
        qadapter['ntasks'] = job_script[job_type]['ntasks']
    elif job_type == 'minhocao_store' or job_type == 'minhopp_store' or job_type == 'train_post':
        job_type = 'train'
        qadapter['nodes'] = 1
        qadapter['ntasks'] = 1
    else:
        qadapter['nodes'] = job_script[job_type]['nodes']
        qadapter['ntasks'] = job_script[job_type]['ntasks']

    if job_script[job_type]['partition']:
        qadapter['queue'] = job_script[job_type]['partition']
    if job_script[job_type]['time']:
        qadapter['time'] = str(job_script[job_type]['time'])
    if job_script[job_type]['launching_mode']:
        if job_script[job_type]['launching_mode'] == 'multiple':
            if job_script[job_type]['number_of_workers']:
                qadapter['rocket_launch'] = ' rlaunch -c ' +  config_dir + ' multi ' +  str(job_script[job_type]['number_of_workers'])
            else:
                return False
        elif job_script[job_type]['launching_mode'] == 'rapidfire':
            qadapter['rocket_launch'] = ' rlaunch -c ' +  config_dir + ' rapidfire'
        elif job_script[job_type]['launching_mode'] == 'singleshot':
            qadapter['rocket_launch'] = ' rlaunch -c ' +  config_dir + ' singleshot'
        else:
            return False
    return qadapter

def submit_jobs(job_type, qadapter, l_dir):
    fworker = FWorker.from_file(os.path.join(config_dir,'my_fworker.yaml'))
    job_script = config_list['job_script']
    q_type = qadapter.q_type
    if q_type == 'SLURM':
        while len(launchpad.get_fw_ids(query={'state': 'READY'})) > 0 or len(launchpad.get_fw_ids(query={'state': 'WAITING'})) > 0:
            job_to_be_submitted = len(launchpad.get_fw_ids(query={'state': 'READY'})) + len(launchpad.get_fw_ids(query={'state': 'WAITING'}))
            cmd = 'squeue --format %j'
            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell = True)
            job_names = proc.stdout.read().decode("utf-8").split('\n')
            job_numbers = job_names.count(qadapter['job_name'])

            if job_type == 'aver_dist' or job_type == 'minhocao_store' or job_type == 'minhopp_store'\
                                       or job_type == 'train_pre' or job_type == 'train_post':
                nqueue = 1-job_numbers
            else:
                nqueue = job_script[job_type]['number_of_jobs']-job_numbers
            if nqueue > 0:
                if nqueue > job_to_be_submitted:
                    nqueue = job_to_be_submitted
                rapidfire(launchpad, fworker, qadapter, launch_dir=l_dir, njobs_queue=0, nlaunches=nqueue, reserve=False)
            sleep(180)
    else:
        if job_type == 'aver_dist' or job_type == 'minhocao_store' or job_type == 'minhopp_store'\
                                   or job_type == 'train_pre' or job_type == 'train_post':
            nqueue = 1
        else:
            nqueue = job_script[job_type]['number_of_jobs']
        rapidfire(launchpad, fworker, qadapter, launch_dir=l_dir, njobs_queue=nqueue, nlaunches=0, sleep_time=180, reserve=False)
    
def run_exists():
    if launchpad.get_fw_ids(query={'state': 'RUNNING'}) or launchpad.get_fw_ids(query={'state': 'WAITING'}):
        return True
    return False

def lp_state(wfname_list):
    wf_state_list = []
    for wfname in wfname_list:
        wfid = launchpad.get_wf_ids({"name": wfname})
        if len(wfid) == 0:
            return 'unknown'
        for a_wfid in wfid:
            wf_state = launchpad.get_wf_summary_dict(a_wfid,mode="less")['state']
            wf_state_list.append(wf_state)
    if 'FIZZLED' in wf_state_list:
        return 'FIZZLED'
    return True

def fizzle_lostruns(ftime):
    launchpad.detect_lostruns(expiration_secs = ftime, fizzle = True, refresh = True)

def fizzle_lostruns_task(ftime):
    while True:
        launchpad.detect_lostruns(expiration_secs = ftime, fizzle = True, refresh = True)
        if len(launchpad.get_fw_ids(query={'state': 'RUNNING'})) > 1:
            sleep(300)
        else:
            break
    fw_ids = []
    fwids = launchpad.get_fw_ids(query={'state': 'WAITING'})
    fw_ids.extend(fwids)
    fwids = launchpad.get_fw_ids(query={'state': 'READY'})
    fw_ids.extend(fwids)
    if len(fw_ids) > 0:
        launchpad.delete_fws(fw_ids)

def rerun():
    fwids = []
    fwids_r = launchpad.get_fw_ids(query={"state":"READY"})
    fwids.extend(fwids_r)
    if fwids:
        wf =  launchpad.get_wf_by_fw_id(fwids[0])
        if wf:
            wf_name = wf.name
            if 'step-2' in wf_name or 'job_control' in wf_name:
                run_jobs('geopt',output_dir)
            elif 'VASP_NSW0' in wf_name:
                run_jobs('nsw0', output_dir)
            elif 'flame' in wf_name:
                stpnm = wf_name.split('_')[1]
                if stpnm == 'train' or stpnm == 'minhocao' or stpnm == 'minhopp' or stpnm == 'divcheck':
                    run_jobs(stpnm, output_dir)
            else:
                return False
    else:
        return False

def find_block_folder():
    block_dir_list = []
    for root, dirs, files in os.walk(output_dir):
        for a_dir in dirs:
            if 'block' in a_dir:
                block_dir_list.append(os.path.join(root,a_dir))
    return block_dir_list

def clean_after_run():
    block_dir_list = find_block_folder()
    if len(block_dir_list) > 0:
        block_dir = block_dir_list[0]
        if input_list['keep_all_files']:
            try:
                os.mkdir(debug_dir)
            except FileExistsError:
                pass
            for file_name in os.listdir(block_dir):
                shutil.move(os.path.join(block_dir,file_name), os.path.join(debug_dir,file_name))
        else:
            for file_name in os.listdir(block_dir):
                shutil.rmtree(os.path.join(block_dir,file_name))

def extend_t():
    with open('task.json','r') as f:
        task_f =  yaml.load(f, Loader=yaml.FullLoader)
    e_time = task_f['run_stats']['overall']['Elapsed time (sec)']
    if e_time < 61:
        sleep(61-e_time)
