import os
import sys
from datetime import datetime
from structure.core import write_compositions_elements, get_known_structures
from workflows.check import previous_run_exist_check
from workflows.core import launchpad_reset
from workflows.config import *

def step_0():
    try:
        os.mkdir(output_dir)
    except FileExistsError:
        pass
    with open(log_file, 'a') as f:
        f.write('Starting PyFLAME'+'\n')
        f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
    try:
        os.mkdir(random_structures_dir)
    except FileExistsError:
        with open(log_file, 'a') as f:
            f.write(">>> Cannot proceed: {} exists <<<".format(random_structures_dir)+'\n')
        sys.exit()
    # reset LPAD
    previous_run_exist_check()
    if not launchpad_reset():
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: cannot reset Launchpad <<<'+'\n')
        sys.exit()
    else:
        with open(log_file, 'a') as f:
            f.write('Launchpad was reset'+'\n')
    # get compositions and elements
    composition_list = input_list['Chemical_formula']
    if len(input_list['Chemical_formula']) > 0:
        write_compositions_elements(composition_list)
    else:
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: no composition is provided <<<'+'\n')
        sys.exit()
    # get_known_structures
    get_known_structures(composition_list)

    return steps_status[0]
