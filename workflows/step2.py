import os
import sys
import json
import numpy as np
from time import sleep
from datetime import datetime
from random import sample
from collections import defaultdict
from fireworks import Firework, PyTask
from pymatgen.core.structure import Structure, Molecule
from VASP.core import get_reference_optimization_wf, get_scheme1_optimization_wf, get_scheme2_optimization_wf, get_cluster_optimization_wf
from structure.core import read_composition_list, get_allowed_n_atom_for_compositions, read_bulk_structure_from_db
from workflows.core import add_wf, run_jobs, fizzle_lostruns, run_exists
from workflows.check import previous_run_exist_check, block_folder_check, launchpad_status_check
from workflows.config import *

def step_2():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 2'+'\n')
        f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

    previous_run_exist_check()
    block_folder_check(should_exist=False, should_be_empty=True)

    try:
        os.mkdir(Flame_dir)
    except:
        pass
    try:
        os.mkdir(os.path.join(Flame_dir,'step-0'))
    except:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot procceed: {} exists <<<'.format(os.path.join(Flame_dir,'step-0'))+'\n')
        sys.exit()
    os.mkdir(os.path.join(Flame_dir,'step-0','task_files'))
    os.mkdir(os.path.join(Flame_dir,'step-0','excluded_task_files'))

    # get compositions and elements
    composition_list = read_composition_list()
    if not composition_list:
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: no composition is provided <<<'+'\n')
        sys.exit()

    allowed_n_atom = get_allowed_n_atom_for_compositions(composition_list)

    if len(input_list['bulk_number_of_atoms']) > 0 and input_list['max_number_of_bulk_structures'] != 0 and len(allowed_n_atom) > 0:
        n_struct_geopt = int(input_list['max_number_of_bulk_structures']/len(allowed_n_atom))
    else:
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: no data for step 2 is provided <<<'+'\n')
        sys.exit()

    allowed_n_atom_reference = []
    for r_n_a in input_list['reference_number_of_atoms']:
        if r_n_a in allowed_n_atom:
            allowed_n_atom_reference.append(r_n_a)

    if input_list['max_number_of_reference_structures'] != 0 and len(allowed_n_atom_reference) > 0 :
        n_struct_geopt_reference = int(input_list['max_number_of_reference_structures']/len(allowed_n_atom_reference))
    else:
        n_struct_geopt_reference = 0
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no reference structure for optimization <<<'+'\n')

    # read known structures if any
    known_bulk_structures_dict = defaultdict(list)
    if os.path.exists(os.path.join(random_structures_dir,'known_bulk_structures.json')):
        with open(log_file, 'a') as f:
            f.write('Reading known bulk structures'+'\n')
        with open(os.path.join(random_structures_dir,'known_bulk_structures.json'), 'r') as f:
            k_b_s = json.loads(f.read())
        for a_k_b_s in k_b_s:
            known_bulk_structures_dict[len(Structure.from_dict(a_k_b_s).sites)].append(Structure.from_dict(a_k_b_s))

    all_bulk_structures_dict = defaultdict(list)
    if input_list['read_structure_from_local_db']:
        with open(log_file, 'a') as f:
            f.write('Reading bulk structures from the local database'+'\n')
        db_bulk_structures_dict = read_bulk_structure_from_db(composition_list, input_list['format_of_structures'], input_list['anonymous_formula'])
        for keys in db_bulk_structures_dict.keys():
            if keys in allowed_n_atom:
                all_bulk_structures_dict[keys].extend(db_bulk_structures_dict[keys])

    if input_list['random_structure_generation'] != 0:
        with open(log_file, 'a') as f:
            f.write('Reading random bulk structures'+'\n')
        if os.path.exists(os.path.join(random_structures_dir,'random_bulk_structures.json')):
            with open(os.path.join(random_structures_dir,'random_bulk_structures.json'), 'r') as f:
                random_bulk_structures_dict = json.loads(f.read())
            for keys in random_bulk_structures_dict.keys():
                if int(keys) in allowed_n_atom:
                    all_bulk_structures_dict[int(keys)].extend(random_bulk_structures_dict[keys])
        else:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: no random bulk structure was found <<<'+'\n')
            sys.exit()

    structure_rfnc_geopt = []
    structure_mtd1_geopt = []
    structure_mtd2_geopt = []

    for a_n_a in all_bulk_structures_dict.keys():
        indices_rfnc_geopt = []
        indices_mtd1_geopt = []
        indices_mtd2_geopt = []

        indices = list(range(len(all_bulk_structures_dict[a_n_a])))

        if a_n_a in input_list['reference_number_of_atoms'] and n_struct_geopt_reference > 0:
            if len(known_bulk_structures_dict[a_n_a]) < n_struct_geopt_reference:
                indices_rfnc_geopt = sample(indices, n_struct_geopt_reference-len(known_bulk_structures_dict[a_n_a]))
                for i in indices_rfnc_geopt:
                    structure_rfnc_geopt.append(Structure.from_dict(all_bulk_structures_dict[a_n_a][i]))
                structure_rfnc_geopt.extend(known_bulk_structures_dict[a_n_a])
            else:
                structure_rfnc_geopt.extend(sample(known_bulk_structures_dict[a_n_a],n_struct_geopt_reference))

            for rem in indices_rfnc_geopt:
                indices.remove(rem)
        if a_n_a in input_list['bulk_number_of_atoms']:
            n_struct_mtd1_geopt = int(0.5 * n_struct_geopt)
            n_struct_mtd2_geopt = n_struct_geopt - n_struct_mtd1_geopt
            if len(indices) >= n_struct_mtd1_geopt:
                indices_mtd1_geopt = sample(indices, n_struct_mtd1_geopt)
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: not enough structures with {} atoms for optimization with scheme 1 <<<'.format(str(a_n_a))+'\n')
                indices_mtd1_geopt = indices

            for rem in indices_mtd1_geopt:
                indices.remove(rem)

            if len(indices) >= n_struct_mtd2_geopt:
                indices_mtd2_geopt = sample(indices, n_struct_mtd2_geopt)
            else:
                with open(log_file, 'a') as f:
                    f.write(' >>> WARNING: not enough structures with {} atoms for optimization with scheme 2 <<<'.format(str(a_n_a))+'\n')
                indices_mtd2_geopt = indices

            for rem in indices_mtd2_geopt:
                indices.remove(rem)

            for i in indices_mtd1_geopt:
                structure_mtd1_geopt.append(Structure.from_dict(all_bulk_structures_dict[a_n_a][i]))
            for i in indices_mtd2_geopt:
                structure_mtd2_geopt.append(Structure.from_dict(all_bulk_structures_dict[a_n_a][i]))


    if input_list['cluster_calculation']:
        cluster_geopt = []
        for a_struct in structure_mtd1_geopt+structure_mtd2_geopt:
            if len(a_struct.sites) in input_list['cluster_number_of_atoms']:
                spcs = []
                crdnts = a_struct.cart_coords
                for coord in a_struct.sites:
                    spcs.append(coord.species_string)
                array_crdnts = np.array(crdnts)
                maxx = max(array_crdnts[:,0:1])[0]
                minx = min(array_crdnts[:,0:1])[0]
                maxy = max(array_crdnts[:,1:2])[0]
                miny = min(array_crdnts[:,1:2])[0]
                maxz = max(array_crdnts[:,2:3])[0]
                minz = min(array_crdnts[:,2:3])[0]
                a_cluster = maxx-minx+input_list['vacuum_length']
                b_cluster = maxy-miny+input_list['vacuum_length']
                c_cluster = maxz-minz+input_list['vacuum_length']
                if a_cluster > input_list['box_size'] or b_cluster > input_list['box_size'] or c_cluster > input_list['box_size']:
                    continue
                molecule = Molecule(spcs, crdnts)
                boxed_molecule = molecule.get_boxed_structure(a_cluster,b_cluster,c_cluster)
                cluster_geopt.append(boxed_molecule)

    fizzle_lostruns_pytask = PyTask(func='workflows.core.fizzle_lostruns_task', args = [3600])
    firework = Firework(fizzle_lostruns_pytask, name = 'job_control', spec={'_priority': 1})
    add_wf(firework)

    with open(log_file, 'a') as f:
        f.write('Ab-initio calculations with VASP'+'\n')

    # add workflows
    if len(structure_rfnc_geopt) > 0:
        step2_rfnc_wf = get_reference_optimization_wf(structure_rfnc_geopt, wf_name = 'step-2')
        for a_wf in step2_rfnc_wf:
            add_wf(a_wf)

    if len(structure_mtd1_geopt) + len(structure_mtd2_geopt) == 0:
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: no bulk structure for optimization <<<'+'\n')
        sys.exit()
    if len(structure_mtd1_geopt) > 0:
        step2_mtd1_wf = get_scheme1_optimization_wf(structure_mtd1_geopt, wf_name = 'step-2')
        for a_wf in step2_mtd1_wf:
            add_wf(a_wf)
    if len(structure_mtd2_geopt) > 0:
        step2_mtd2_wf = get_scheme2_optimization_wf(structure_mtd2_geopt, wf_name = 'step-2')
        for a_wf in step2_mtd2_wf:
            add_wf(a_wf)
    if len(cluster_geopt) > 0:
        step2_cluster_wf = get_cluster_optimization_wf(cluster_geopt, wf_name = 'step-2')
        for a_wf in step2_cluster_wf:
            add_wf(a_wf)

    # run jobs
    if not run_jobs('geopt',output_dir):
        with open(log_file, 'a') as f:
            f.write('>>> Cannot run jobs for VASP geometry optimization. Check config.yaml <<<'+'\n')
        sys.exit()
    # wait until all jobs are done
    while True:
        fizzle_lostruns(3600)
        if run_exists():
            sleep(60)
        else:
            break
    # check launchpad state
    launchpad_status_check(['step-2'], exit_if_fizlled = False)

    with open(log_file, 'a') as f:
        f.write('STEP 2 ended'+'\n')
        f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
    return steps_status[2]
