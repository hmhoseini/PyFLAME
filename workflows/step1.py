import sys
from time import sleep
from datetime import datetime
from flame.flame_workflows import get_gensymcrys_wf
from structure.workflows import get_pyxtal_wf
from structure.core import read_composition_list
from workflows.core import run_jobs, clean_after_run, add_wf, run_exists, fizzle_lostruns
from workflows.check import previous_run_exist_check, block_folder_check, launchpad_status_check
from workflows.config import *

def step_1():
    with open(log_file, 'a') as f:
        f.write('STEP 1'+'\n')
        f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

    if os.path.exists(os.path.join(random_structures_dir,'random_bulk_structures.json')):
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(random_structures_dir,'random_bulk_structures.json'))+'\n')
        sys.exit()

    previous_run_exist_check()
    block_folder_check(should_exist=False, should_be_empty=True)

    if input_list['random_structure_generation'] != 0:
        # get compositions and elements
        composition_list = read_composition_list()
        if not composition_list:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: no composition is provided <<<'+'\n')
            sys.exit()
        # get workflow
        if input_list['random_structure_generation'] == 1:
            with open(log_file, 'a') as f:
                f.write('random structure generation with gensymcrys'+'\n')
            step1_wf = get_gensymcrys_wf(composition_list)

        elif input_list['random_structure_generation'] == 2:
            with open(log_file, 'a') as f:
                f.write('random structure generation with PyXtal'+'\n')
            step1_wf = get_pyxtal_wf(composition_list)
        else:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: cannot proceed with random structure generation <<<'+'\n')
            sys.exit()

        with open(log_file, 'a') as f:
            f.write('For more details see {}/random_structure.dat'.format(random_structures_dir)+'\n')

        # add wf to lp
        add_wf(step1_wf)
        # run
        if not run_jobs('random_struct_gen', output_dir):
            with open(log_file, 'a') as f:
                f.write('>>> Cannot run jobs for random structure generation. Check config.yaml <<<'+'\n')
            sys.exit()
        # wait until all jobs are done
        while True:
            fizzle_lostruns(3600)
            if run_exists():
                sleep(60)
            else:
                break

        # check launchpad state
        launchpad_status_check(['step-1'], exit_if_fizlled = True)
        # mv/rm calculation files
        clean_after_run()
    else:
        with open(log_file, 'a') as f:
            f.write('Nothing to do!'+'\n')
    with open(log_file, 'a') as f:
        f.write('STEP 1 ended'+'\n')
        f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
    return steps_status[1]
