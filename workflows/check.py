import os
import sys
from fireworks.core.launchpad import LaunchPad
from workflows.core import lp_state, find_block_folder
from workflows.config import log_file, config_dir

launchpad = LaunchPad.from_file(os.path.join(config_dir,'my_launchpad.yaml'))

def previous_run_exist_check():
    if launchpad.get_fw_ids(query={'state': 'RUNNING'}) or\
       launchpad.get_fw_ids(query={'state': 'WAITING'}) or\
       launchpad.get_fw_ids(query={'state': 'READY'}):
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
        sys.exit()

def launchpad_status_check(wfn_list, exit_if_fizlled = False):
    launchpad_state = lp_state(wfn_list)
    if launchpad_state == 'FIZZLED':
        if exit_if_fizlled:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: Workflow is FIZZLED <<<'+'\n')
            sys.exit()
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
    elif launchpad_state == 'unknown':
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
        sys.exit()

def block_folder_check(should_exist=True, should_be_empty=True):
    block_dir_list = find_block_folder()
    if should_exist and len(block_dir_list) == 0:
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: calculation directory was not found <<<'+'\n')
        sys.exit()

    if len(block_dir_list) > 1:
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
        sys.exit()

    if should_be_empty and len(block_dir_list) != 0:
        block_dir = block_dir_list[0]
        if len(os.listdir(block_dir)) != 0:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: {} is not empty <<<'.format(block_dir)+'\n')
            sys.exit()
