import os
import sys
import shutil
import json
import math
from time import sleep
from datetime import datetime
from pymatgen.core.structure import Structure
from pymatgen.io.vasp.outputs import Vasprun
from VASP.core import get_nsw0_wf
from structure.failed_structures import get_pertured_failed_structures
from workflows.check import previous_run_exist_check, block_folder_check, launchpad_status_check
from flame.aver_dist import compute_aver_dist
from flame.divcheck import collect_divcheck_results
from flame.flame_workflows import *
from structure.core import *
from workflows.core import *
from workflows.config import *

def step_4():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 4'+'\n')
    # calculate the aver_dist for pickdiff if it is not already calculated
    if not os.path.exists(os.path.join(Flame_dir,'aver_dist','aver_dist.json')):
        with open(log_file, 'a') as f:
            f.write("-----------------------------------------------"+'\n')
            f.write('Aaverage distance calculation'+'\n')
            f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
        previous_run_exist_check()
        block_folder_check(should_exist=False, should_be_empty=True)

        # creating directories
        try:
            os.mkdir(os.path.join(Flame_dir,'aver_dist'))
        except FileExistsError:
            with open(log_file, 'a') as f:
                f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,'aver_dist'))+'\n')
            sys.exit()

        # get compositions and elements
        composition_list = read_composition_list()
        if not composition_list:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: no composition is provided <<<'+'\n')
            sys.exit()

        # get average distance wf
        aver_dist_wf = get_aver_dist_wf(composition_list)
        # add workflow
        add_wf(aver_dist_wf)
        # run average distance jobs
        if not run_jobs('aver_dist', output_dir):
            with open(log_file, 'a') as f:
                f.write('>>> Cannot run jobs for average distance calculations. Check config.yaml <<<'+'\n')
            sys.exit()
        # wait until all jobs are done
        while True:
            fizzle_lostruns(3600)
            if run_exists():
                sleep(60)
            else:
                break
        # check launchpad status
        launchpad_status_check(['aver_dist'], exit_if_fizlled=False)
        block_folder_check(should_exist=True, should_be_empty=False)

        # calculae average distances and store it
        compute_aver_dist()
        # mv/rm calculation files
        clean_after_run()

        with open(log_file, 'a') as f:
            f.write('Aaverage distance calculation ended'+'\n')
            f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
####################FLAME LOOP####################
    FLAME_step_name = restart['FLAME_loop_start'][1]
    for c_s_n in range(restart['FLAME_loop_start'][0], restart['FLAME_loop_stop'][0]+1):
        step_number = 'step-'+str(c_s_n)
######FLAME train######
        if FLAME_step_name == 'train':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('cycle-{}: train calculations'.format(c_s_n)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            if len(input_list['number_of_nodes']) < c_s_n:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: parameters for step-{} {} are not provided <<<'.format(c_s_n, FLAME_step_name)+'\n')
                sys.exit()
            previous_run_exist_check()
            block_folder_check(should_exist=False, should_be_empty=True)

            # creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number))
            except FileExistsError:
                pass
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'train'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,step_number,'train'))+'\n')
                sys.exit()

            # get train_pre wf
            wf_name = 'flame_train_pre_cycle_'+str(c_s_n)
            flame_train_pre_wf = get_train_pre_wf(step_number, wf_name)
            # add workflow
            add_wf(flame_train_pre_wf)
            # run train jobs
            if not run_jobs('train_pre', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run jobs for training. Check config.yaml <<<'+'\n')
                sys.exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(7200)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            launchpad_status_check([wf_name], exit_if_fizlled = True)

            # get train wf
            wf_name = 'flame_train_cycle_'+str(c_s_n)
            flame_train_wf = get_train_wf(step_number, wf_name)
            # add workflow
            add_wf(flame_train_wf)
            # run train jobs
            if not run_jobs('train', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run jobs for training. Check config.yaml <<<'+'\n')
                sys.exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(14400)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            launchpad_status_check([wf_name], exit_if_fizlled = False)

            # get train_post wf
            wf_name = 'flame_train_post_cycle_'+str(c_s_n)
            flame_train_post_wf = get_train_post_wf(step_number, wf_name)
            # add workflow
            add_wf(flame_train_post_wf)
            # run train jobs
            if not run_jobs('train_post', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run jobs for training. Check config.yaml <<<'+'\n')
                sys.exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(7200)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            launchpad_status_check([wf_name], exit_if_fizlled = True)
            # mv/rm calculation files
            clean_after_run()

            with open(log_file, 'a') as f:
                f.write('cycle-{}: train calculations ended'.format(c_s_n)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'train':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                sys.exit()
            else:
                FLAME_step_name = 'minhocao'
######FLAME minhocao######
        if FLAME_step_name == 'minhocao':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('cycle-{}: minhocao calculations'.format(c_s_n)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            if len(input_list['bulk_minhocao']) < c_s_n or\
               len(input_list['minhocao_steps']) < c_s_n:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: parameters for step-{} {} are not provided <<<'.format(c_s_n, FLAME_step_name)+'\n')
                sys.exit()

            previous_run_exist_check()
            block_folder_check(should_exist=False, should_be_empty=True)

            # creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'minhocao'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,step_number,'minhocao'))+'\n')
                sys.exit()

            # get minhocao wf
            wf_name = 'flame_minhocao_cycle_'+str(c_s_n)
            flame_minhocao_wfs = get_minhocao_wf(step_number, wf_name)
            if len(flame_minhocao_wfs) > 0:
                # add workflow
                for a_wf in flame_minhocao_wfs:
                    add_wf(a_wf)
                # run minhocao jobs
                if not run_jobs('minhocao', output_dir):
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot run jobs for minhocao. Check config.yaml <<<'+'\n')
                    sys.exit()
                # wait until all jobs are done
                while True:
                    fizzle_lostruns(3600)
                    if run_exists():
                        sleep(60)
                    else:
                        break
                # check launchpad status
                launchpad_status_check([wf_name], exit_if_fizlled = False)
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no minhocao calculations <<<'+'\n')
                FLAME_step_name = 'minhopp'

            with open(log_file, 'a') as f:
                f.write('cycle-{}: minhocao calculations ended'.format(c_s_n)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'minhocao':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                sys.exit()
            elif FLAME_step_name == 'minhocao':
                FLAME_step_name = 'minhocao_store'
######FLAME minhocao_store######
        if FLAME_step_name == 'minhocao_store':

            previous_run_exist_check()
            block_folder_check(should_exist=True, should_be_empty=False)

            with open(log_file, 'a') as f:
                f.write('cycle-{}: reading minhocao results'.format(c_s_n)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # get minhocao_store wf
            wf_name = 'minhocao_store_cycle_'+str(c_s_n)
            minhocao_store_wf = get_minhocao_store_wf(step_number, wf_name)
            # add workflow
            add_wf(minhocao_store_wf)
            # run minhocao jobs
            if not run_jobs('minhocao_store', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot store minhocao results. Check config.yaml <<<'+'\n')
                sys.exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(14400)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            launchpad_status_check([wf_name], exit_if_fizlled = True)
            # mv/rm calculation files
            clean_after_run()

            with open(log_file, 'a') as f:
                f.write('minhocao results are stored'+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'minhocao_store':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                sys.exit()
            else:
                FLAME_step_name = 'minhopp'
######FLAME minhopp######
        if FLAME_step_name == 'minhopp':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('cycle-{}: minhopp calculations'.format(c_s_n)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            if len(input_list['bulk_minhopp']) < c_s_n or\
               len(input_list['minhopp_steps']) < c_s_n:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: parameters for step-{} {} are not provided <<<'.format(c_s_n, FLAME_step_name)+'\n')
                sys.exit()

            if input_list['cluster_calculation'] and len(input_list['cluster_minhopp']) < c_s_n:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: parameters for step-{} {} are not provided <<<'.format(c_s_n, FLAME_step_name)+'\n')
                sys.exit()

            previous_run_exist_check()
            block_folder_check(should_exist=False, should_be_empty=True)

            # creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'minhopp'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,step_number,'minhopp'))+'\n')
                sys.exit()

            # get minhopp wf
            wf_name = 'flame_minhopp_cycle_'+str(c_s_n)
            flame_minhoppc_wfs, flame_minhoppb_wfs = get_minhopp_wf(step_number, wf_name)
            # add workflows
            if len(flame_minhoppb_wfs) > 0:
                for a_wf in flame_minhoppb_wfs:
                    add_wf(a_wf)
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no minhopp calculations for bulk structures <<<'+'\n')
            if input_list['cluster_calculation']:
                if len(flame_minhoppc_wfs) > 0:
                    for a_wf in flame_minhoppc_wfs:
                        add_wf(a_wf)
                else:
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: no minhopp calculations for clusters <<<'+'\n')
            if len(flame_minhoppb_wfs) + len(flame_minhoppc_wfs) > 0:
                # run minhopp jobs
                if not run_jobs('minhopp', output_dir):
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot run jobs for minhopp. Check config.yaml <<<'+'\n')
                    sys.exit()
                # wait until all jobs are done
                while True:
                    fizzle_lostruns(3600)
                    if run_exists():
                        sleep(60)
                    else:
                        break
                # check launchpad status
                launchpad_status_check([wf_name], exit_if_fizlled = False)
            else:
                FLAME_step_name = 'divcheck'

            with open(log_file, 'a') as f:
                f.write('cycleE-{}: minhopp calculations ended'.format(c_s_n)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'minhopp':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                sys.exit()
            elif FLAME_step_name == 'minhopp':
                FLAME_step_name = 'minhopp_store'
######FLAME minhopp_store######
        if FLAME_step_name == 'minhopp_store':

            previous_run_exist_check()
            block_folder_check(should_exist=True, should_be_empty=False)

            with open(log_file, 'a') as f:
                f.write('cycle-{}: reading minhopp results'.format(c_s_n)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # get minhopp_store wf
            wf_name = 'minhopp_store_cycle_'+str(c_s_n)
            minhopp_store_wf = get_minhopp_store_wf(step_number, wf_name)
            # add workflow
            add_wf(minhopp_store_wf)
            # run minhopp jobs
            if not run_jobs('minhopp_store', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run minhopp store job. Check config.yaml <<<'+'\n')
                sys.exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(7200)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            launchpad_status_check([wf_name], exit_if_fizlled = True)
            # mv/rm calculation files
            clean_after_run()

            with open(log_file, 'a') as f:
                f.write('minhopp results are stored'+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'minhopp_store':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                sys.exit()
            else:
                FLAME_step_name = 'divcheck'
######FLAME divcheck######
        if FLAME_step_name == 'divcheck':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('cycle-{}: divcheck calculations'.format(c_s_n)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            previous_run_exist_check()
            block_folder_check(should_exist=False, should_be_empty=True)

            # creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'divcheck'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,step_number,'divcheck'))+'\n')
                sys.exit()
            # get divcheck workflow
            wf_name = 'flame_divcheck_cycle_'+str(c_s_n)
            flame_divcheck_b_wf = get_divcheck_b_wf(step_number, wf_name)
            # add workflow
            if len(flame_divcheck_b_wf) > 0:
                add_wf(flame_divcheck_b_wf)
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no divcheck for bulk structures <<<'+'\n')
            # for cluster
            if input_list['cluster_calculation']:
                # get divcheck workflow
                wf_name = 'flame_divcheck_cycle_'+str(c_s_n)
                flame_divcheck_c_wf = get_divcheck_c_wf(step_number, wf_name)
                # add workflow
                if len(flame_divcheck_c_wf) > 0:
                    add_wf(flame_divcheck_c_wf)
                else:
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: no divcheck for clusters <<<'+'\n')
            # run divcheck jobs
            if not run_jobs('divcheck', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run divcheck jobs. Check config.yaml <<<'+'\n')
                sys.exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(3600)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            launchpad_status_check([wf_name], exit_if_fizlled = True)
            # mv/rm calculation files
            clean_after_run()

            with open(log_file, 'a') as f:
                f.write('cycle-{}: divcheck calculations ended'.format(c_s_n)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'divcheck':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                sys.exit()
            else:
                FLAME_step_name = 'DFT_SP_calculations'
###FLAME single point calculations
        if FLAME_step_name == 'DFT_SP_calculations':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write("cycle-{}: VASP single point calculations".format(c_s_n)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            previous_run_exist_check()
            block_folder_check(should_exist=False, should_be_empty=True)

            SP_bulk_structures, SP_clusters = collect_divcheck_results(step_number)
            perturbed_bstructs, perturbed_cstructs = get_pertured_failed_structures(step_number)

            wf_name_list = []
            for keys in SP_bulk_structures:
                if len(SP_bulk_structures[keys]) > 0:
                    wfn = 'VASP_NSW0_'+str(keys)+'-atom_bulk_cycle_'+str(c_s_n)
                    vasp_nsw0_bulk_wfs = get_nsw0_wf(SP_bulk_structures[keys], wfn, 'bulk')
                    for a_wf in vasp_nsw0_bulk_wfs:
                        add_wf(a_wf)
                    wf_name_list.append(wfn)
                else:
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: no atomic configuration for {}-atom bulk structures <<<'.format(str(keys))+'\n')

            if len(perturbed_bstructs) > 0:
                wfn = 'VASP_NSW0_failed_bulk_structures'
                vasp_nsw0_failed_bulk_wfs = get_nsw0_wf(perturbed_bstructs, wfn, 'bfailed')
                for a_wf in vasp_nsw0_failed_bulk_wfs:
                    add_wf(a_wf)
                wf_name_list.append(wfn)

            if input_list['cluster_calculation']:
                for keys in SP_clusters:
                    if len(SP_clusters[keys]) > 0:
                        wfn = 'VASP_NSW0_'+str(keys)+'-atom_cluster_cycle_'+str(c_s_n)
                        vasp_nsw0_cluster_wfs = get_nsw0_wf(SP_clusters[keys], wfn, 'cluster')
                        for a_wf in vasp_nsw0_cluster_wfs:
                            add_wf(a_wf)
                        wf_name_list.append(wfn)
                    else:
                        with open(log_file, 'a') as f:
                            f.write('>>> WARNING: no atomic configuration for {}-atom clusters <<<'.format(str(keys))+'\n')
                if len(perturbed_cstructs) > 0:
                    wfn = 'VASP_NSW0_failed_clusters'
                    vasp_nsw0_failed_cluster_wfs = get_nsw0_wf(perturbed_cstructs, wfn, 'cfailed')
                    for a_wf in vasp_nsw0_failed_cluster_wfs:
                        add_wf(a_wf)
                    wf_name_list.append(wfn)

            # run vasp jobs
            if not run_jobs('nsw0', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run VASP single point calculations. Check config.yaml <<<'+'\n')
                sys.exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(3600)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            launchpad_status_check(wf_name_list, exit_if_fizlled = False)

            with open(log_file, 'a') as f:
                f.write('cycle-{}: VASP single point calculations ended'.format(c_s_n)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'DFT_SP_calculations':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                sys.exit()
            else:
                FLAME_step_name = 'data_collection'
###single point calculations collecting_data
        if FLAME_step_name == 'data_collection':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('cycle-{} data collection'.format(c_s_n)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            previous_run_exist_check()
            block_folder_check(should_exist=True, should_be_empty=False)

            # creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'task_files'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,step_number,'task_files')+'\n'))
                sys.exit()
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'excluded_task_files'))
            except FileExistsError:
                pass

            block_dir = find_block_folder()[0]

            with open(os.path.join(random_structures_dir,'min_epa.dat'), 'r') as f:
                min_epa = float(f.readline().strip())
            e_window = input_list['energy_window']
            next_step_minhocao_structs = []
            next_step_minhopp_clusters = []
            next_step_minhopp_bulks = []
            for root, dirs, files in os.walk(block_dir):
                if 'task.json' in files and 'vasprun.xml.gz' in files:
                    try:
                        with open(os.path.join(root,'task.json'), 'r') as f:
                            a_task_file = json.loads(f.read())
                    except:
                        with open(log_file, 'a') as f:
                            f.write('cannot open task file in {}'.format(root)+'\n')
                        continue
                    try:
                        vasprun = Vasprun(os.path.join(root,'vasprun.xml.gz'))
                    except:
                        continue
                    if not vasprun.converged_electronic:
                        continue

                    task_label = a_task_file['task_label']
                    epa = a_task_file['output']['energy_per_atom']
                    if epa < min_epa + e_window:
                        shutil.copyfile(os.path.join(root,'task.json'),\
                                        os.path.join(Flame_dir,step_number,'task_files',"task" + str(epa) + '.json'))
                        # select and store structures for next step minhocao
                        if 'bulk' in task_label:
                            if epa < min_epa:
                                min_epa = epa
                                with open(os.path.join(random_structures_dir,'min_epa.dat'), 'w') as f:
                                    f.write(str(min_epa))
                            struct = Structure.from_dict(a_task_file['output']['structure'])
                            forces = a_task_file['output']['forces']
                            stress = a_task_file['output']['stress']
                            tot_forces = []
                            for j in range(0,len(forces)):
                                tot_forces.append(math.sqrt(forces[j][0]**2 + forces[j][1]**2 + forces[j][2]**2))
                            max_tot_foce = max(tot_forces)
                            external_pressure = (stress[0][0]+stress[1][1]+stress[2][2])/3
                            if max_tot_foce < 0.71:
                                if external_pressure > -50 and external_pressure < 50:
                                    next_step_minhocao_structs.append(struct.as_dict())
                                if external_pressure < -50 and external_pressure > 50:
                                    next_step_minhopp_bulks.append(struct.as_dict())
                        if 'cluster' in task_label:
                            a_cluster = Structure.from_dict(a_task_file['output']['structure'])
                            forces = a_task_file['output']['forces']
                            tot_forces = []
                            for j in range(len(forces)):
                                tot_forces.append(math.sqrt(forces[j][0]**2 + forces[j][1]**2 + forces[j][2]**2))
                            max_tot_foce = max(tot_forces)
                            if max_tot_foce < 1.01:
                                lattice = [[input_list['box_size'],0,0],[0,input_list['box_size'],0],[0,0,input_list['box_size']]]
                                n_cluster = Structure(lattice, a_cluster.species, a_cluster.cart_coords, coords_are_cartesian=True)
                                next_step_minhopp_clusters.append(n_cluster.as_dict())
                    else:
                        shutil.copyfile(os.path.join(root,'task.json'),\
                                        os.path.join(Flame_dir,step_number,'excluded_task_files',"task" + str(epa) + '.json'))

            if len(next_step_minhocao_structs) > 0:
                with open(os.path.join(Flame_dir,step_number,'minhocao','next-step_minhocao_seeds.json'),'w') as f:
                    json.dump(next_step_minhocao_structs,f)
            with open(log_file, 'a') as f:
                f.write('Number of structures for the next step minhocao: {}'.format(len(next_step_minhocao_structs))+'\n')

            if len(next_step_minhopp_clusters) > 0:
                with open(os.path.join(Flame_dir,step_number,'minhopp','next-step_minhopp_cluster_seeds.json'),'w') as f:
                    json.dump(next_step_minhopp_clusters,f)
            with open(log_file, 'a') as f:
                f.write('Number of clusters for the next step minhopp: {}'.format(len(next_step_minhopp_clusters))+'\n')
            if len(next_step_minhopp_bulks) > 0:
                with open(os.path.join(Flame_dir,step_number,'minhopp','next-step_minhopp_bulk_seeds.json'),'w') as f:
                    json.dump(next_step_minhopp_bulks,f)
            with open(log_file, 'a') as f:
                f.write('Number of bulks for the next step minhopp: {}'.format(len(next_step_minhopp_bulks))+'\n')

            # mv/rm calculation files
            clean_after_run()
            with open(log_file, 'a') as f:
                f.write('VASP collecting data ended'+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'data_collection':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                sys.exit()
            else:
                FLAME_step_name = 'train'
