import os
import sys
import json
import shutil
from datetime import datetime
from collections import defaultdict
from pymatgen.core.structure import Structure, Molecule
from pymatgen.io.vasp.outputs import Vasprun
from structure.core import is_structure_valid
from workflows.core import find_block_folder, clean_after_run
from workflows.check import previous_run_exist_check, block_folder_check
from workflows.config import *

def step_3():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 3'+'\n')
        f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

    previous_run_exist_check()
    block_folder_check(should_exist=True, should_be_empty=False)
    block_dir = find_block_folder()[0]

    if not os.path.exists(os.path.join(Flame_dir,'step-0','task_files')):
        with open(log_file, 'a') as f:
            f.write('>>> Cannot procceed: {} does not exist <<<'.format(os.path.join(Flame_dir,'step-0','task_files'))+'\n')
        sys.exit()
    if not os.path.exists(os.path.join(Flame_dir,'step-0','excluded_task_files')):
        with open(log_file, 'a') as f:
            f.write('>>> Cannot procceed: {} does not exist <<<'.format(os.path.join(Flame_dir,'step-0','excluded_task_files'))+'\n')
        sys.exit()
    if len(os.listdir(os.path.join(Flame_dir,'step-0','task_files'))) != 0:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot procceed: {} is not empty <<<'.format(os.path.join(Flame_dir,'step-0','task_files'))+'\n')
        sys.exit()
    if len(os.listdir(os.path.join(Flame_dir,'step-0','excluded_task_files'))) != 0:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot procceed: {} is not empty <<<'.format(os.path.join(Flame_dir,'step-0','excluded_task_files'))+'\n')
        sys.exit()

    structs_epa = []

    for root, dirs, files in os.walk(block_dir):
        if 'task.json' in files and 'vasprun.xml.gz' in files:
            try: 
                with open(os.path.join(root,'task.json'), 'r') as f:
                    s = json.loads(f.read())
            except:
                continue
            try:
                vasprun = Vasprun(os.path.join(root,'vasprun.xml.gz'))
            except:
                continue
            if not vasprun.converged:
                continue

            if 'bulk' in s["task_label"] or 'cluster' in s["task_label"]:
                if s['output']['energy_per_atom'] < 0:
                    structs_epa.append(s['output']['energy_per_atom'])

    min_epa = min(structs_epa)
    max_epa = max(structs_epa)
    ave_epa = sum(structs_epa)/len(structs_epa)

    # store energy/atom
    with open(os.path.join(random_structures_dir,'epa.dat'), 'w') as f:
        f.write('min_epa: {}'.format(min_epa)+'\n')
        f.write('max_epa: {}'.format(max_epa)+'\n')
        f.write('ave_epa: {}'.format(ave_epa)+'\n')
    with open(os.path.join(random_structures_dir,'min_epa.dat'), 'w') as f:
        f.write(str(min_epa))

    minhopp_cluster_seeds = []
    minhopp_bulk_seeds = []
    minhopp_bulk_compressed_seeds = []
    minhopp_bulk_expanded_seeds = []
    minhocao_seeds = []

    aver_dist_structs_dict = defaultdict(list)

    e_window = input_list['energy_window']

    for root, dirs, files in os.walk(block_dir):
        if 'task.json' in files and 'vasprun.xml.gz' in files:
            try:
                with open(os.path.join(root,'task.json'), 'r') as f:
                    s = json.loads(f.read())
            except:
                continue
            try:
                vasprun = Vasprun(os.path.join(root,'vasprun.xml.gz'))
            except:
                continue
            if not vasprun.converged:
                continue

            if 'bulk' in s["task_label"] or 'cluster' in s["task_label"]:
                epa = s['output']['energy_per_atom']
                nat = len(Structure.from_dict(s['output']['structure']).sites)
                if epa < min_epa + e_window and is_structure_valid(Structure.from_dict(s['output']['structure']), False, True, False):
                    new_task_name = "task" + str(epa) + '.json'
                    shutil.copyfile(os.path.join(root,'task.json'), os.path.join(Flame_dir,'step-0','task_files',new_task_name))
                    # seeds for minhopp and minhocao
                    if nat in input_list['bulk_number_of_atoms'] and s["task_label"] == 'bulk' or s["task_label"] == 'nsw0_bulk':
                        minhocao_seeds.append(s['output']['structure'])
                    if nat in input_list['cluster_number_of_atoms'] and 'cluster' in s["task_label"]:
                        structure_from_task = Structure.from_dict(s['output']['structure'])
                        spcs = []
                        crdnts = structure_from_task.cart_coords
                        for coord in structure_from_task.sites:
                            spcs.append(coord.species_string)
                        molecule = Molecule(spcs, crdnts)
                        try:
                            boxed_cluster = molecule.get_boxed_structure(input_list['box_size'],input_list['box_size'],input_list['box_size'])
                            minhopp_cluster_seeds.append(boxed_cluster.as_dict())
                        except ValueError:
                            pass
                    if nat in input_list['bulk_number_of_atoms'] and s["task_label"] == 'scaled_bulk' or s["task_label"] == 'opt2_bulk':
                        stress = s['output']['stress']
                        external_pressure = (stress[0][0]+stress[1][1]+stress[2][2])/3
                        if external_pressure > 50:
                            minhopp_bulk_seeds.append(s['output']['structure'])
                            minhopp_bulk_compressed_seeds.append(s['output']['structure'])
                        if external_pressure < -50:
                            minhopp_bulk_seeds.append(s['output']['structure'])
                            minhopp_bulk_expanded_seeds.append(s['output']['structure'])
                        if external_pressure > -50 and external_pressure < 50:
                            minhocao_seeds.append(s['output']['structure'])
                    if 'bulk' in s["task_label"]:
                        aver_dist_structs_dict[nat].append(s['output']['structure'])
                else:
                    new_task_name = "excluded" + str(epa) + '.json'
                    shutil.copyfile(os.path.join(root,'task.json'), os.path.join(Flame_dir,'step-0','excluded_task_files',new_task_name))

    # store minhopp seeds
    with open(os.path.join(random_structures_dir,'minhopp_cluster_seeds.json'),'w') as f:
        json.dump(minhopp_cluster_seeds, f)
    with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
        f.write('{} seeds for minhopp cluster are stored.'.format(len(minhopp_cluster_seeds))+'\n')

    with open(os.path.join(random_structures_dir,'minhopp_bulk_seeds.json'),'w') as f:
        json.dump(minhopp_bulk_seeds, f)
    with open(os.path.join(random_structures_dir,'minhopp_bulk_compressed_seeds.json'),'w') as f:
        json.dump(minhopp_bulk_compressed_seeds, f)
    with open(os.path.join(random_structures_dir,'minhopp_bulk_expanded_seeds.json'),'w') as f:
        json.dump(minhopp_bulk_expanded_seeds, f)

    with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
        f.write('{} seeds for minhopp bulk are stored: {} compressed and {} expanded'.format(len(minhopp_bulk_seeds), len(minhopp_bulk_compressed_seeds), len(minhopp_bulk_expanded_seeds))+'\n')

    # store minhocao seeds
    with open(os.path.join(random_structures_dir,'minhocao_seeds.json'),'w') as f:
        json.dump(minhocao_seeds, f)
    with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
        f.write('{} seeds for minhocao are stored.'.format(len(minhocao_seeds))+'\n')

    # store aver_dist structures
    with open(os.path.join(random_structures_dir,'aver_dist_structures.json'), 'w') as f:
        json.dump(aver_dist_structs_dict, f)

    # mv/rm calculation files
    clean_after_run()

    with open(log_file, 'a') as f:
        f.write('STEP 3 ended'+'\n')
        f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
    return steps_status[3]
