import os
import json
import pymongo
from fireworks.fw_config import CONFIG_FILE_DIR

def config_set(run_dir):

    config_dir = CONFIG_FILE_DIR

    with open(os.path.join(config_dir,'db.json'), 'r') as f:
        db = json.loads(f.read())

    myclient = pymongo.MongoClient(db['host'], username =  db['admin_user'], password = db['admin_password'])
    mydb = myclient["path"]
    mycol = mydb["directories"]
    mycol.delete_many({})
    dir_dict = {"run_dir": run_dir}
    mycol.insert_one(dir_dict)
