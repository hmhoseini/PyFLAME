# Manual


## About ##
PyFLAME introduces an automated workflow for training neural network interatomic potentials (NNPs) with [FLAME](https://gitlab.com/flame-code/FLAME).

## Dependencies ##
**Note:** Python 3.6 or later is required.

[FLAME](https://gitlab.com/flame-code/FLAME) (the latest version of FLAME compatible with Python 3 is required)

[atomate](https://atomate.org/installation.html) is used heavily in this project and needs to be connected to [MongoDB](https://www.mongodb.com/).

## Optional packages ##
[PyXtal](https://github.com/qzhu2017/PyXtal) can be used for random structure generation.


## Download and installation ##


The source code can be downloaded by

git clone https://gitlab.com/hmhoseini/PyFLAME.git


There is no installation command. However, the PyFLAME directory should be set in PYTHONPATH.

## Usage ##

The directory structure of PyFLAME directory is as follows:

PyFLAME <br>
&nbsp;&nbsp;|--- examples <br>
&nbsp;&nbsp;|--- flame <br>
&nbsp;&nbsp;|--- run_dir <br>
&nbsp;&nbsp;|--- structure <br>
&nbsp;&nbsp;|--- templates <br>
&nbsp;&nbsp;|--- VASP <br>
&nbsp;&nbsp;|--- workflows

The default output directory is "run_dir" but pyflame.py can be executed in any directory that contains the following files: <br>
&nbsp;&nbsp; config.yaml <br>
&nbsp;&nbsp; input.yaml <br>
&nbsp;&nbsp; restart.yaml <br>
It is necessary to modify these three files before running PyFLAME.

In addition to the values that are listed in the following table, **config.yaml** contains the name of template files for submitting VASP and FLAME jobs. Examples of queue templates can be found [here](https://github.com/materialsproject/fireworks/tree/main/fireworks/user_objects/queue_adapters) and in PyFLAME/templates folder. The following values can/should be specified in the config.py file:

|Values                |Description|
|-|-|
|job_name|The name of jobs submitted by PyFLAME (optional)|
|ntasks-per-node| See [slurm manual](https://slurm.schedmd.com/sbatch.html#OPT_ntasks-per-node) (necessary)|
|account |See [slurm manual](https://slurm.schedmd.com/sbatch.html#OPT_account)|
|partition|See [slurm manual](https://slurm.schedmd.com/sbatch.html#OPT_partition)|
|time| Set a limit on the total run time of the submitted jobs|
|||
|number_of_jobs|The maximum number of jobs that will be submitted for an specifc job type. It should be specified for all jobs.|
|nodes|Number of nodes to be allocate for the job. See [slurm manual](https://slurm.schedmd.com/sbatch.html#OPT_nodes)|
|ntasks|Number of tasks|
|partition|See [slurm manual](https://slurm.schedmd.com/sbatch.html#OPT_ntasks)|
|time|If it is not specified in default section then it should be specified for every job.|
|launching_mode|singleshot, rapidfire, or multiple. See [here](https://materialsproject.github.io/fireworks/introduction.html) and [here](https://materialsproject.github.io/fireworks/multi_job.html?highlight=multiple)|
|number_of_workers|If the launching_mode is set to multiple, this keyword specifies the number of workers. See [here](https://materialsproject.github.io/fireworks/multi_job.html?highlight=multiple)|




The data that should be provided in **input.yaml** is as follows:

|Key|Description|
|-|-|
|chemical_formula|A list of chemical formula(s)|
|||
|random_structure_generation|Specifies the method for random structure generation. 0 for no random structure, 1 for gensymcrys (implemented in FLAME), and 2 for PyXtal|
|bulk_number_of_atoms|Specifies number of atoms in bulk structures|
|max_number_of_bulk_structures|Specifies number of structures sent for ab-initio calculations|
|||
|reference_number_of_atoms|Specifies number of atoms in reference structures. Should be a subset of bulk_number_of_atoms. The structures are optimized with tight criteria and if large number of atoms is give, then it could be time consuming.|
|max_number_of_reference_structures|Specifies number of reference structures|
|||
|cluster_calculation|If cluster structures should be included in the training|
|cluster_number_of_atoms|Number of atoms on cluster structures. Should be a subset of bulk_number_of_atoms|
|box_size|Size of the box for clusters in Angstrom|
|vacuum_length|Minimum length of vacuum for each supercell containing a cluster|
|||
|read_structure_from_local_db|If True, then the structures provided in db folder will be used to initiate the training cycle. This folder should be in the same directory that PyFLAME is executed.|
|format_of_structures|Specifies the format of files in the db folder|
|anonymous_formula|If provided structures in db folder do not have the same chemical formula as the chemical formula, then anonymous formula should be True.|
|||
|min_distance_prefactor|The allowed distance between atoms is calculated as the sum of their covalent radii. The allowed distance can be tuned by this prefactor.|
|descending_prefactor|Either False or a number to specify the percentage of descending min_distance_prefactor in each cycle of training|
|||
|method|behler|
|number_of_nodes|Number of nodes in the hidden layer of the NN for each cycle of training.|
|number_of_epoch|Number of epoch for each cycle of training.|
|||
|bulk_minhocao|Maximum number of minhocao calculations. Zero: no minhocao calculation|
|minhocao_time|Minimum and maximum time for mihocao jobs (in hours)|
|minhocao_steps|Maximum number of minhocao steps|
|minhocao_temp|Temperatures for minima hopping calculations|
|minhocao_max_temp|Allowed maximum temperature for minima hopping calculations|
|||
|bulk_minhopp |Maximum number of minhopp calculations for bulks. Zero : no bulk minhopp calculation|
|cluster_minhopp|Maxiumu number of minhopp calculations for clusters. Zero: no cluster minhopp calculation|
|minhopp_time|Minimum and maximum time for minhopp jobs (in hours)|
|minhopp_steps|Maximum number of minhopp steps|
|||
|dtol_prefactor|Prefactor for structure diversity check. The larger the value is, the more structures are considered similar (and are removed from the list).|
|prefactor_cluster|A prefactor for dtol_prefactor to be employed for clusters|
|||
|ab_initio_code|VASP|
|user_specified_VASP_file|If True, then PyFLAME/VASP/vasp_files folder should be copied into the run directory. The user can modify VASP keywords.|
|user_specified_FLAME_files|If True, then PyFLAME/flame/flame_files folder should be copied into the run directory. The user can modify FLAME keywords.|
|keep_all_files|Whether the files in calculation directory should be copied to the debug folder|

<br>
The step from which PyFLAME (re)starts is specified in restart.yaml. The script keeps track of its steps. A detailed log file will be written in the output directory (the default name is pyflame.log). If a failure occurs and PyFLAME cannot advance, the user can restart PyFLAME from the last successfully accomplished step. It is noted that the user can always restart PyFLAME from a previous step with different input parameters.

The following parameters can be specified in **restart.yaml**:

|Key                |Description|
|-|-|
|re-start_from_step |Specifies the step the script will (re)start.<br>-1: runs jobs in the Launchpad <br>0: initiates PyFLAME job<br>1: random bulk structure generation <br>2: add and run jobs for bulk structure optimization <br>3: data collection from bulk structure calculations <br>4: FLAME calculations|
|stop_after_step|The script stops after the end of this step. -1 for non-stop run.|
|FLAME_loop_start|Specifies the step number and step name for the training cycle, if "re-start_from_step: 4".<br> The step names are as follows:<br>train, minhocao, minhocao_store, minhopp, minhopp_store, divcheck, SP_calculations, data_collection|
|FLAME_loop_stop|Specifies the step number and step name to stop training cycle.|

When the above-mentioned files are ready, the script can be executed by running the following command:

**pyflame.py**

<br>

**output directory**

The structure of the output directory is as follows :

&nbsp;&nbsp;|--- FLAME_calculations <br>
&nbsp;&nbsp;&nbsp;&nbsp;|--- aver_dist <br>
&nbsp;&nbsp;&nbsp;&nbsp;|--- step-0 <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  excluded_task_files <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  task_files <br>
&nbsp;&nbsp;&nbsp;&nbsp;|--- step-1 <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  divcheck <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  excluded_task_files <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  minhocao <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  minhopp <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  task_files <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  train <br>
&nbsp;&nbsp;&nbsp;&nbsp;. <br>
&nbsp;&nbsp;&nbsp;&nbsp;. <br>
&nbsp;&nbsp;&nbsp;&nbsp;. <br>
&nbsp;&nbsp;&nbsp;&nbsp;|--- step-n <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  divcheck <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  excluded_task_files <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  minhocao <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  minhopp <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  task_files <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  train <br>
&nbsp;&nbsp;|---  random_structures<br>


### Examples ###
The directory examples contains the constructed NN potentials for (Ti, 4)-(O, -2) and (In, 3)-(Cu, 1)-(Se, -2) systems. The results of [this work](https://www.sciencedirect.com/science/article/pii/S0927025621002949) can be reproduced by these potentials.

## Citation ##

@article{PyFLAME,<br>
title = "An automated approach for developing neural network interatomic potentials with FLAME",<br>
author = "Mirhosseinia, Hossein and Tahmasbib, Hossein and Kuchanaa, Sai Ram  and Ghasemia, S. Alireza and Kühne, Thomas D.",<br>
journal = "",<br>
volume = "197",<br>
pages = "110567",<br>
year = "2021",<br>
issn = "0927-0256",<br>
doi = "https://doi.org/10.1016/j.commatsci.2021.110567",<br>
url = "https://www.sciencedirect.com/science/article/pii/S0927025621002949",<br>
}

## How to contribute ##


## License ##
